jasmine.DEFAULT_TIMEOUT_INTERVAL = 30000
roomForTesting = "1_atlassian@conf.dev-rmanalang1.hipchat.com"

describe "Storage", ->

  it "should allow set and get", ->
    hipchat.store.set('a', 1)
    expect(hipchat.store.get('a')).toBe(1)
    hipchat.store.unset('a')

  it "should return nothing if get finds nothing", ->
    expect(hipchat.store.get('a')).toBe(undefined)

  it "should allow unset", ->
    hipchat.store.set('a', 1)
    expect(hipchat.store.unset('a')).toBe(true)

  it "should allow clear", ->
    hipchat.store.set('a', 1)
    expect(hipchat.store.clear()).toBe(hipchat.store.locations)
    expect(localStorage.length).toBe(0)

  it "should expire key/vals properly", (done)->
    hipchat.store.ttl = 2*1000
    hipchat.store.set('a', 1)
    expect(hipchat.store.get('a')).toBe(1)
    cb = ->
      expect(hipchat.store.get('a')).toBe(undefined)
      done()
    _.delay cb, 3000

describe "Client", ->
  it "should be connected", ->
    expect(hipchat.is_connected).toBe(true)

  it "should allow fetching of initial client state", (done)->
    hipchat.fetchInitData (data)->
      console.info('INIT',data)
      expect(data.query.preferences).toBeDefined()
      done()

describe "Roster", ->
  it "can be fetched", (done)->
    hipchat.fetchRoster (roster)->
      console.info('ROSTER',roster)
      expect(roster.query.item.length).toBeGreaterThan(0)
      expect(roster.query.item[0].name).toBeDefined()
      done()

  xit "presence can be fetched", (done)->
    hipchat.once 'presence', (pres)->
      console.log('ROSTER PRESENCE', pres)
      expect(pres.x.client_type).toBeDefined()
      done()
    hipchat.fetchPresence()

describe "Rooms", ->
  it "should be possible to join a room", (done) ->
    hipchat.joinRoom roomForTesting + '/' + config.user_name, (roomInfo)->
      console.log('ROOMINFO',roomInfo)
      expect(roomInfo.x.history).toBeDefined()
      done()

  it "should allow fetching of all rooms", (done) ->
    hipchat.fetchRooms include_archived=false, (rooms) ->
      console.info('ROOMS',rooms)
      expect(rooms.query.item).toBeDefined()
      done()

  xit 'should allow room change subscriptions', (done)->
    orig_name = _.where(config.prefs.autoJoin,{jid: config.prefs.chatToFocus})[0].name
    new_name = orig_name + ' ' + Math.random()
    hipchat.once 'roomChange', (room) ->
      console.log('RENAMED ROOM TO', new_name,room)
      expect(room.query.item._name).toBe(new_name)

      hipchat.once 'roomChange', (room2) ->
        console.log('RENAMED ROOM BACK TO', orig_name, room2)
        expect(room2.query.item._name).toBe(orig_name)
        done()

      hipchat.renameRoom config.prefs.chatToFocus, orig_name, (data) ->
        expect(typeof data).toBe('object')

    hipchat.renameRoom config.prefs.chatToFocus, new_name, (data)->
      expect(typeof data).toBe('object')
      console.log('CHANGING ROOM NAME', data)

describe "Chat", ->
  it "group history should be fetch-able", (done)->
    hipchat.fetchHistory config.prefs.chatToFocus, null, 1, (history)->
      expect(history.error).toBeUndefined()
    hipchat.once 'message', (message) ->
      console.log('GOT GROUP HISTORY', message)
      expect(message.body).toBeDefined()
      done()

  it "1-1 history should be fetch-able", (done) ->
    hipchat.once 'message', (message) ->
      console.log('GOT 1-1 HISTORY', message)
      expect(message.body).toBeDefined()
      done()
    hipchat.fetchHistory config.user_jid, null, null, (history)->
      expect(history.error).toBeUndefined()

  it "should be allowed to send 1-1 messages", (done)->
    setTimeout ->
      hipchat.sendMessage hipchat.current_user_jid, 'Ping'
      hipchat.once 'message', (msg) ->
        console.log('1-1 MESSAGE RCVD', msg)
        expect(msg.body).toBe('Ping')
        done()
    , 1000

  it "should be allowed to send room messages", (done)->
    setTimeout ->
      hipchat.sendMessage roomForTesting, 'Pong'
      hipchat.once 'message', (msg) ->
        console.log('ROOM MESSAGE RCVD', msg)
        expect(msg.body).toBe('Pong')
        done()
    , 1000

describe "Presence", ->
  it "should be subscribe-able", (done)->
    hipchat.once 'presence', (pres)->
      console.info('PRESENCE',pres)
      done()
    hipchat.setPresence('available', 'dnd', 'working hard', 1000)

describe "Disconnected client", ->

  xit "should be disconnected", ->
    hipchat.disconnect()
    expect(hipchat.is_connected).toBe(false)

  xit "should reconnect", (done)->
    setTimeout ->
      expect(hipchat.is_connected).toBe(true)
      done()
    , hipchat.reconnect_delay+5000
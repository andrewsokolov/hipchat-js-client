# HipChat JS Client

HipChat's BOSH/XMPP communication layer has always been a mystery to me. So, a few weeks ago I set out to educate myself on how it all worked -- this client library is the side-effect.

## What is this?

How about some code?

    // Bootstraps the HipChat connection using your existing
    // HipChat browser session
    new HipChat(
        {
            onConnectionChange: function(status, condition){
                console.log('INFO', 'connection status changed to ' + status + ': ' + condition);
            },

            onConnect: function(hc){
                // Let's join all the rooms that HipChat knows about from
                // the last session
                hc.joinAutoJoinRooms();

                // Let's listen to any messages that float by
                var msgSub = hc.on('message', msgHandler);

                // Let's listen in on any presence notifications
                var presenceSub = hc.on('presence', presenceHandler);
            }
        }
    });

Get the point? If not, check out the [specs](https://bitbucket.org/hipchat/hipchat-js-client/src/master/spec/hipchat_spec.coffee?at=master) to see what else it can do.

## What can I do with this?

This little library will allow you to win the next ShipIt! Here are some ideas:

* Build alternate browser based HipChat clients using Chrome Extensions/Apps
* Build an ncurses like chat app and bring chat back to the terminal
* Integrate HipChat closer to your favorite app

I built this mostly to educate myself and have a bit of fun, but if all goes well, this may be used in the next version of the HipChat web client.

## Specs

To run the specs, you must symlink this to your HipChat VM's web dir (e.g., from ../web/www/js):

    ln -s ../../../hipchat-js-client

You may then access the spec runner at:

    https://<dev-vm-hostname>.hipchat.com/js/hipchat-js-client/specs.html

## Wanna contribute?

Go for it. Fork, commit, push, and be merry.
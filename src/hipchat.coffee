class Storage
  constructor: (opts) ->
    @prefix = 'hc.'
    @expiry_prefix = '_'
    @locations = ['local', 'session', 'global']
    @ttl = opts.ttl || 60*60*24*1000
    @disableStorage = opts.disableStorage
    try
      return 'localStorage' of window && window['localStorage'] is not null
    catch e
      window.localStorage = window.sessionStorage;

  has: (key) =>
    return if @disableStorage
    _.first(@locations, (location) =>
      _.keys(window[(location+'Storage')]).indexOf(@prefix + key) != -1
    )[0]

  get: (key) =>
    return if @disableStorage
    if key.indexOf(@expiry_prefix) != 0
      expiry = @get(@expiry_prefix + key)
      if (new Date).getTime() > expiry
        @unset(key)
        return
    location = @has(key)
    if location
      try
        return JSON.parse(window[(location + 'Storage')].getItem(@prefix + key).toString())
      catch e
        return
    return

  set: (key, val, location) =>
    return if @disableStorage
    window[((location or 'local') + 'Storage')].setItem(@prefix + key, JSON.stringify(val, (k,v)->
      if v instanceof RegExp
        return v.toString()
      v
    ))
    @set(@expiry_prefix+key, (new Date).getTime() + @ttl) if key.indexOf(@expiry_prefix) != 0

  unset: (key) =>
    return if @disableStorage
    location = @has(key)
    @unset(@expiry_prefix + key) if key.indexOf(@expiry_prefix) != 0
    if location
      window[(location + 'Storage')].removeItem(@prefix + key)
      return true
    return false

  clear: () =>
    return if @disableStorage
    _.each @locations, (location) =>
      _.filter _.keys(window[(location + 'Storage')]), (key) =>
        return key.match(@prefix)
      .forEach (key) ->
        window[(location + 'Storage')].removeItem(key)

class HipChat
  constructor: (opts)->
    @baseUrl = opts.baseUrl || ""
    @clientType =  if opts.clientType then '/' + opts.clientType else ''
    @clientVersion = opts.clientVersion or 1
    @is_initial_connect = true
    @do_reconnect = true
    @KEEP_ALIVE_MS = 25000
    @HEARTBEAT_MS = 30 * 1000
    @RECONNECT_BACKOFF_FACTOR = 3
    @RECONNECT_DELAY_MS = 2 * 1000
    @RECONNECT_MAX_DELAY = 60 * 1000
    @MAX_RECONNECT_TIME = 10 * 60 * 1000
    @CACHE_TTL = 90*24*60*60*1000
    @reconnect_delay = @RECONNECT_DELAY_MS
    @reconnect_attempt_number = 0
    @reconnect_time = 0
    @rooms_joined = []
    @cached_profiles = {}
    @cachedRoomInfo = {}
    @token_info = {}
    @noop = ()->{}

    @onInitialConnectCallback = opts.onInitialConnect or @noop
    @onConnectionChangeCallback = opts.onConnectionChange or @noop
    @onConnectionFailCallback = opts.onConnectionFail or @noop
    @onConnectCallback = opts.onConnect or @noop
    @onServerDataCallback = opts.onServerData or @noop
    @onDisconnectCallback = opts.onDisconnect or @noop
    @onReconnectingCallback = opts.onReconnecting or @noop
    @onReconnectionSuccessCallback = opts.onReconnectionSuccess or @noop
    @onReconnectionFailCallback = opts.onReconnectionFail or @noop
    @onUserIdleCallback = opts.onUserIdle or @noop
    @onUserActiveCallback = opts.onUserActive or @noop
    @onStaleSessionCallback = opts.onStaleSession or @noop
    @onMaxReconnectAttemts = opts.onMaxReconnectAttemts or @noop
    @disableStorage = opts.disableStorage or false

    @store = new Storage
      ttl: @CACHE_TTL
      disableStorage: @disableStorage

    @previous_presence = @current_presence =
      show: 'chat'
      status: null
      type: 'available'

    log_prefix = '[HipChatJS]'
    Strophe.log = (level, msg)->
      if typeof window != 'undefined' and 'console' of window and window.HC_LOG
        switch level
          when Strophe.LogLevel.DEBUG then console.debug log_prefix, msg
          when Strophe.LogLevel.INFO then console.info log_prefix, msg
          when Strophe.LogLevel.WARN then console.warn log_prefix, msg
          when Strophe.LogLevel.ERROR, Strophe.LogLevel.FATAL then console.error log_prefix, msg

    @x2js = new X2JS

    @init = (data)=>
      window.location.reload() if data.error
      window.config = data
      @store.clear() if @store.get('uid') != config.user_id
      @store.set('uid',config.user_id)
      @config = data
      if data.prefs.isIdleTimeEnabled
        if !data.prefs.secondsToIdle || data.prefs.secondsToIdle && data.prefs.secondsToIdle < 900 then data.prefs.secondsToIdle = 900
        @IDLE_DELAY_MS = data.prefs.secondsToIdle * 1000
      else
        @IDLE_DELAY_MS = 60*60*24*365*1000
      @setupConnection()
      @connect()

      # run once
      @setupIdleEvents()

      $(window).on 'beforeunload', =>
        @disconnect(true) if @is_connected

    # privates
    @parsedCallback = (data, once, cb) =>
      if typeof once == 'function'
        cb = once
        once = false

      cb @x2js.xml2json(data)
      not once

    @clearData = ->
      @room_rosters = []
      @room_history_loaded = {}
      @room_loaded = {}
      @roster = []
      @roster_loaded = false

    @randomBetween = (min, max) =>
      Math.floor(Math.random() * (max - min + 1)) + min

    @decorrelatedJitter = (cap, base, sleep, backoff_factor) =>
      Math.min(cap, @randomBetween(base, sleep * backoff_factor))

    @refreshReconnectTimer = ()=>
      if @is_connected
        @reconnect_timer = null
      else if @do_reconnect
        @reconnect()

    @scheduleReconnectAttempt = () ->
      @reconnect_delay = @decorrelatedJitter(@RECONNECT_MAX_DELAY, @RECONNECT_DELAY_MS, @reconnect_delay, @RECONNECT_BACKOFF_FACTOR)
      @reconnect_timer = setTimeout(_.bind(@refreshReconnectTimer), @reconnect_delay)

    @sendIQ = (stanza, cb)=>
      @connection.sendIQ stanza.tree(), (data)=>
        @parsedCallback(data, cb)
      , (err) =>
        @parsedCallback(err, cb)

    @subscribe = (evt, once=false, opts=null, cb)=>
      if evt == 'roomChange'
        evt = 'iq'
        type = 'set'
        ns = 'jabber:client'
      else if evt is 'joinRoom' or evt is 'createRoom'
        evt = 'presence'
        type = null
        ns = null
      else
        type = null
        ns = null

      if opts
        id = opts.id
        from = opts.from

      @connection.addHandler (data)=>
        @parsedCallback data, once, cb
      ,ns, evt, type, id, from

    @toXMPPDate = (dttm)->
      return dttm if typeof dttm == 'string'
      (new Date(dttm.setMilliseconds(0))).toISOString().replace(/\.000/,'')

    @getJidType = (jid)->
      re = new RegExp(config.conference_server)
      if re.test(jid)
        'groupchat'
      else
        'chat'

    @getJidFromNickname = (nickname) =>
      if /\//.test(nickname)
        nickname = nickname.split('/')[1]

      rslts = _.where @cached_profiles,
        query:
          name: nickname
      rslts[0]

    @callIfCached = (key, cb) ->
      cached_val = @store.get(key)
      if cached_val
        cb(cached_val)
        @xmlListenerCallback(cached_val)
      cached_val

    @monkeypatchStropheForHipchatAuth = (successCallback) ->
      clientType = @clientType
      clientVersion = @clientVersion

      # First monkeypatch the authenticate method
      Strophe.Connection.prototype.authenticate = (matched) ->
        @_sasl_success_handler = @_addSysHandler(successCallback.bind(@), null, "success", null, null);
        @_sasl_failure_handler = @_addSysHandler(@_sasl_failure_cb.bind(@), null, "failure", null, null);
        @_sasl_challenge_handler = @_addSysHandler(@_sasl_challenge_cb.bind(@), null, "challenge", null, null);

        if config.auth_method is 'nonce'
          request_auth_exchange = $build("auth", {
            xmlns: Strophe.NS.SASL,
            mechanism: 'X-HIPCHAT-WEB'
            nonce: config.auth_nonce
            oauth2_token: 'true'
            node: 'http://hipchat.com/client/web' + clientType
            ver: clientVersion
          })
          request_auth_exchange.t(config.user_jid.split('@')[0]);
        else if config.auth_method is 'oauth2'
          request_auth_exchange = $build("auth", {
            xmlns: 'http://hipchat.com',
            mechanism: 'oauth2'
            oauth2_token: 'true'
            node: 'http://hipchat.com/client/web' + clientType
            ver: clientVersion
          })
          request_auth_exchange.t(Base64.encode('\u0000' + config.auth_token + '\u0000web'));
        else if config.auth_method is 'password'
          request_auth_exchange = $build("auth", {
            xmlns: 'http://hipchat.com',
            oauth2_token: 'true'
            node: 'http://hipchat.com/client/web' + clientType
            ver: clientVersion
          })
          request_auth_exchange.t(Base64.encode('\u0000' + config.auth_email + '\u0000' + config.auth_password+ '\u0000web'));

        @send(request_auth_exchange.tree());

      # Next monkeypatch the downstream auth callback (to do no binding)
      Strophe.Connection.prototype._sasl_auth1_cb = (elem) ->
        @authenticated = true;
        @_changeConnectStatus(Strophe.Status.CONNECTED, null);

      # Finally, monkeypatch the XHR factory method to enable withCredentials property
      originalXHRFactory = Strophe.Request.prototype._newXHR;
      Strophe.Request.prototype._newXHR = () ->
        xhr = (originalXHRFactory.bind(@))();
        xhr.withCredentials = true;
        return xhr;

    @hipchatAuthSuccessCallback = (elem) ->
      # Pull relevant info from the success stanza
      config.oauth2_token = elem.getAttribute('oauth2_token');
      @jid = elem.textContent;

      oldRestartMethod = @_sendRestart;

      # If we're not doing nonce auth, don't reset the stream (because of some weird conditionals in the server code)
      (@_sendRestart = () -> {}) unless config.auth_method is 'nonce'
      returnVal = @_sasl_success_cb(elem);
      @_sendRestart = oldRestartMethod;

      # If we're not doing nonce auth, jump right to the next callback step (since were not resetting the stream)
      @_sasl_auth1_cb(null) unless config.auth_method is 'nonce'

      return returnVal;

    if opts.initState and not @baseUrl
      @init opts.initState
    else
      $.ajax
        url: @baseUrl + '/chat/session?nonce_auth=1'
        dataType: 'json'
        success: @init


  onConnectChange: (status, condition)=>

    if status == Strophe.Status.CONNECTING
      @connection_failed = false
      Strophe.debug('[Strophe] is connecting]')

    else if status == Strophe.Status.CONNFAIL
      Strophe.debug('[Strophe] Offline. Condition: '+condition)
      @connection_failed = true
      @onConnectionFail(condition)

    else if status == Strophe.Status.DISCONNECTING
      Strophe.debug('[Strophe] is disconnecting. Condition: '+condition)

    else if status == Strophe.Status.DISCONNECTED
      Strophe.debug('[Strophe] is disconnected.')
      @onDisconnect()

    else if status == Strophe.Status.CONNECTED
      Strophe.debug('[Strophe] is connected.')
      @connection_failed = false
      @onConnect()

    else if status == Strophe.Status.ATTACHED
      Strophe.debug('[Strophe] is attached.')
      @connection_failed = false

    else if status == Strophe.Status.ERROR
      Strophe.error('[Strophe] encountered error: ' + condition)

    @onConnectionChangeCallback(status, condition)

  _fixXhtmlMessageBody: (message, raw) =>
    messageExtractor = new RegExp("mid=['\"]" + message.mid + "['\"]\.+?</message>", "i")
    xhtmlBodyExtractor = new RegExp("<body[^>]*xmlns=[\"']http:\\/\\/www\\.w3\\.org\\/1999\\/xhtml[\"'][^>]*>(.*?)<\/body>", "i")
    if message?.html?.body?.xmlns is 'http://www.w3.org/1999/xhtml'
      matches = messageExtractor.exec(raw)
      if matches?.length
        rawMessage = matches[0]
        matches = xhtmlBodyExtractor.exec(rawMessage)
        if matches?.length > 1
          message.html.body.__text = matches[1]

  attachXmlListener: (cb = @noop) =>
    @xmlListenerCallback = cb
    json = null

    # These callbacks assume that strophe sends raw input after xml input
    @connection.xmlInput = (elem)=>
      @last_xmpp_activity = (new Date).getTime()
      json = @x2js.xml2json(elem)

    @connection.rawInput = (raw)=>
      if json?.message
        messageArray = if Array.isArray(json.message) then json.message else [json.message]
        @_fixXhtmlMessageBody(messageObj, raw) for messageObj in messageArray
      cb json

  onUserIdle: =>
    return unless @is_connected
    Strophe.info('Going idle')
    @is_idle = true
    @setPresence 'away', @current_presence.status, (@IDLE_DELAY_MS / 1000)
    @onUserIdleCallback(@)

  onUserActive: ->
    return unless @is_connected
    Strophe.info 'Returning from idle', @previous_presence, @current_presence
    @is_idle = false
    @setPresence @previous_presence.show, @previous_presence.status
    @onUserActiveCallback(@)

  onUserAction: (event) =>
    clearTimeout @idle_timeout
    @onUserActive() if @is_idle
    @idle_timeout = setTimeout(_.bind(@onUserIdle), @IDLE_DELAY_MS)
    @throttledKeepAlive()

  setupIdleEvents: ->
    $(document).on 'mousemove.idle keydown.idle DOMMouseScroll.idle mousewheel.idle mousedown.idle', _.bind(@onUserAction)
    @idle_timeout = setTimeout(_.bind(@onUserIdle), @IDLE_DELAY_MS)
    @check_session_timer = setInterval(_.bind(@checkSession), @HEARTBEAT_MS)
    @throttledKeepAlive = _.throttle(() =>
      return if @isXMPPActive()
      @keepAlive('userAction')
    , @HEARTBEAT_MS)

  isXMPPActive: =>
    return false unless @last_xmpp_activity
    ((new Date).getTime() - @last_xmpp_activity) > @HEARTBEAT_MS

  isXMPPSessionExpired: =>
    return false unless @last_xmpp_activity
    age = ((new Date).getTime() - @last_xmpp_activity)
    if (age > @KEEP_ALIVE_MS)
      age
    else
      false

  checkSession: =>
    if @session_age = @isXMPPSessionExpired()
      Strophe.debug('[Strophe] Stale session detected. Session age: ' + @session_age)
      @onStaleSessionCallback(@session_age)
      @keepAlive('checkSession')

  updateIdleTime: (newIdleTime)->
    clearTimeout(@idle_timeout)
    clearInterval(@check_session_timer)
    @idle_timeout = null
    $(document).off '.idle'
    @IDLE_DELAY_MS = newIdleTime * 1000
    @setupIdleEvents()

  keepAlive: (origin) =>
    return unless @is_connected
    Strophe.debug("[Strophe] Stayin' alive! Called from " + origin)
    ping = $iq(
      type: 'get'
      to: config.chat_server
    ).c('ping',
      xmlns: 'urn:xmpp:ping'
    )
    if @connection
      @connection.send ping
    else
      return
    true

  setupConnection: ->
    if @config.auth_method
      @monkeypatchStropheForHipchatAuth(@hipchatAuthSuccessCallback)

    if @connection
      @connection.disconnect('resetting before reconnect')
      @connection.reset()
    else
      @connection = new Strophe.Connection(config.bind_url)

  connect: (isReconnection = false) =>
    connectChangeWrapper = (status, condition)=>
      if isReconnection
        @onReconnectionSuccessCallback(true) if status is Strophe.Status.CONNECTED
        @onReconnectionFailCallback(data) if status in [Strophe.Status.AUTHFAIL, Strophe.Status.CONNFAIL]
      @onConnectChange(status, condition)

    if @config.auth_method
      Strophe.debug('[Strophe] connect()')
      @connection.connect(config.jid, null, connectChangeWrapper, null, null, config.route) unless @is_connected
    else
      Strophe.debug('[Strophe] attach()')
      @connection.attach(config.jid, config.sid, config.rid, connectChangeWrapper) unless @is_connected
      @onConnect()

  onConnect: =>
    @do_reconnect = true
    @is_connected = true

    clearTimeout(@reconnect_timer)
    @reconnect_timer = null

    @reconnect_delay = @RECONNECT_DELAY_MS
    @reconnect_attempt_number = 0
    @reconnect_time = 0

    # @current_jid = @lobby_jid
    @current_user_jid = config.user_jid
    @current_user_nickname_jid = config.user_name + '@' + config.chat_server

    @attachXmlListener(@onServerDataCallback)
    @setPresence null, null, null, true, true

    if @is_initial_connect
      @fetchInitData()
      @onInitialConnectCallback(@)

    @is_initial_connect = false

    @onConnectCallback(@)

    # if config.is_guest
    # @setPresence 'available'
    # @getAPIAuthToken()
    # @getEmoticons()

  onDisconnect: =>
    @is_connected = false
    @refreshReconnectTimer() unless @reconnect_timer
    @onDisconnectCallback(@)

  disconnect: (should_not_reconnect)=>
    Strophe.debug('[Strophe] disconnect()')
    if should_not_reconnect
      @do_reconnect = false

    @clearData()
    @connection.disconnect() if @connection
    @connection.deleteTimedHandler(@timed_handler) if @timed_handler and @connection
    @connection = null

    clearTimeout @auth_token_refresh_timer
    @auth_token_refresh_timer = null

  onConnectionFail: (condition) =>
    @is_connected = false

    switch condition
      when 'conflict'
        @do_reconnect = false
        break
      when 'not-allowed'
        location.reload(true)
        break
      when 'plan-change'
        break
      when 'system-shutdown'
        break
      when 'see-other-host'
        break

    @onConnectionFailCallback(condition)

  reconnect: (cb = @noop) ->
    return if @is_connected
    Strophe.debug('[Strophe] reconnect(). Next attempt in ' + @reconnect_delay/1000 + 's')
    @onReconnectingCallback(@reconnect_attempt_number, @reconnect_delay)

    if @reconnect_time >= @MAX_RECONNECT_TIME
      @onMaxReconnectAttemts(@)
      return

    @reconnect_attempt_number++
    @reconnect_time += @reconnect_delay
    opts =
      is_guest: config.is_guest
      guest_key: config.guest_key
      uid: config.user_id
      nonce_auth: if config.auth_method is 'nonce' then 1 else 0

    errHandler = (data)=>
      @onReconnectionFailCallback(data)
      cb('fail')
      if data.refresh_page == true
        window.location.reload()

    successCb = (data)=>
      if data and not data.error
        window.config = data
        @config = data
        @clearData()
        @setupConnection()
        @connect(true)
        cb('success')
      else if data and data.error
        errHandler(data)
      @scheduleReconnectAttempt()
    errCb = (jqXHR, textStatus, errorThrown) =>
      errHandler(jqXHR)
      Strophe.error 'Error trying to reconnect. Status: ' + textStatus + ' -- error: ' + errorThrown
      if (typeof navigator == 'undefined' || navigator.onLine)
        @onConnectionChangeCallback(Strophe.Status.DISCONNECTED)
        @do_reconnect = false
      else
        @scheduleReconnectAttempt()

    $.ajax
      url: @baseUrl + '/chat/session'
      dataType: 'json'
      data: opts
      success: successCb
      error: errCb

  on: (evt, opts, cb = @noop) =>
    if typeof opts == 'function'
      cb = opts
      opts = null
    @subscribe evt, false, opts, cb

  once: (evt, opts, cb = @noop)=>
    if typeof opts == 'function'
      cb = opts
      opts = null
    @subscribe evt, true, opts, cb

  off: (subscriptionRef) ->
    @connection.deleteHandler(subscriptionRef)

  setPresence: (show, status, idle, send_caps, roster_presences = false) ->
    return if not @is_connected

    show = show or 'chat'
    Strophe.info 'Setting presence: ' + ' - ' + show + ' - ' + idle

    stanza = $pres {
      roster_presences: roster_presences.toString()
    }
    stanza.c('show').t(show).up() if show and show != 'chat'
    stanza.c('status').t(status).up() if typeof status is 'string'
    if idle
      stanza.c 'query',
        xmlns: 'jabber:iq:last'
        seconds: idle
      .up()
    if send_caps
      stanza.c 'c',
        xmlns: 'http://jabber.org/protocol/caps'
        node: 'http://hipchat.com/client/web' + @clientType
        ver: @clientVersion
      .up()
    @connection.send stanza.tree()

    @previous_presence = @current_presence
    @current_presence =
      type: 'available'
      show: show
      status: status
      idle: idle
      send_caps: send_caps

  unsubscribeFromPresences: () ->
    stanza = $pres
      roster_presences: 'false'
    @connection.send stanza.tree()

  leaveRoom: (jid, type, message) ->
    if type == 'chat'
      return @sendStateMessage(jid, type, 'gone');
    stanza = $pres
      to: jid + '/' + config.user_name
      type: 'unavailable'
    if message
      stanza.c('status').t(message)

    @connection.send stanza.tree()

  fetchAPIv1AuthToken: (cb = @noop)->
    dt = new Date
    if @token_info and @token_info.expiration > dt.getTime()
      this.token_info.token
    stanza = $iq(type: 'get', to: config.chat_server)
      .c 'query',
        xmlns: 'http://hipchat.com/protocol/auth'
    @sendIQ stanza, (d) =>
      @token_info =
        token: d.query.token['__text']
        expiration: (parseInt(d.query.token.expiration, 10)*1000)
      @auth_token_refresh_timer = setTimeout @fetchAPIv1AuthToken.bind(@), (@token_info.expiration - dt.getTime())+5000
      cb(@token_info)

  fetchInitData: (cb = @noop)->
    stanza = $iq(type: 'get', id: Math.random()*10000000|0)
      .c 'query',
        xmlns: 'http://hipchat.com/protocol/startup'
        send_auto_join_user_presences: true
    @sendIQ stanza, (d) =>
      dt = new Date
      @token_info =
        token: d.query.token['__text']
        expiration: (parseInt(d.query.token.expiration, 10)*1000)
      @auth_token_refresh_timer = setTimeout @fetchAPIv1AuthToken.bind(@), (@token_info.expiration - dt.getTime())+5000
      cb(d)

  fetchEmoticons: (cb = @noop) ->
    cached_val = @callIfCached('emoticons', cb)
    stanza = $iq(type: 'get')
      .c 'query',
        xmlns: 'http://hipchat.com/protocol/emoticons'
        ver: if cached_val and cached_val.query then cached_val.query.ver else ''
    @sendIQ stanza, (d)=>
      if d.query
        cb(d)
        if !d.error
          @store.set('emoticons', iq: d)

  fetchRoster: (cb = @noop, cbCached)->
    cached_val = @callIfCached('roster', cb)
    stanza = $iq(type: 'get')
      .c 'query',
        xmlns: 'jabber:iq:roster'
        ver: if cached_val and cached_val.query then cached_val.query.ver else ''
    @sendIQ stanza, (d)=>
      if d.query
        cb(d)
        if !d.error
          @store.set('roster', iq: d)

  fetchPresence: (uid = [], cb = @noop)->
    if !Array.isArray(uid) then uid = [uid]
    stanza = $iq(type: 'get')
      .c 'query',
        xmlns: 'http://hipchat.com/protocol/presence'
    uid.forEach( (id) ->
      @c('uid', id).up()
    , stanza)

    @sendIQ stanza, cb

  limitPresencesToUsers: (uid = [], id = _.uniqueId(), cb = @noop)->
    if !Array.isArray(uid) then uid = [uid]
    stanza = $iq
        type: 'set'
        id: id
      .c 'query',
        xmlns: 'http://hipchat.com/protocol/presence',
        action: 'presence_filter'
    uid.forEach( (id) ->
      @c('uid', id).up()
    , stanza)

    @sendIQ stanza, cb

  inviteUsersToRoom: (room_jid, user_jids = [], reason = '', id = null)->
    if !Array.isArray(user_jids) then user_jids = [user_jids]
    attrs =
      to: room_jid
      id: Math.random()*10000000|0
    if id != null then attrs['id'] = id
    node = $msg(attrs)
    .c 'x',
      xmlns: 'http://jabber.org/protocol/muc#user'
    user_jids.forEach( (jid) ->
      @c('invite', to: jid)
      @c('reason')
      @t(reason)
      @up()
      @up()
    , node)
    @connection.send(node.tree())

  removeUsersFromRoom: (room_jid, user_jids = [], cb = @noop)->
    if !Array.isArray(user_jids) then user_jids = [user_jids]
    Strophe.info 'Removing users from room'
    stanza = $iq
      type: 'set'
      to: room_jid
    .c 'query',
      xmlns: 'http://hipchat.com'
    user_jids.forEach((jid) ->
      @c('item', jid: jid, affiliation: "none")
      @up()
    , stanza)
    @sendIQ stanza, cb

  fetchRooms: (ignore_archived=true, cb = @noop) ->
    Strophe.info 'Getting room list'
    key = if !ignore_archived then 'rooms-w-archived' else 'rooms-wo-archived'
    @callIfCached(key, cb)
    stanza = $iq
      type: 'get'
      to: config.conference_server
    .c 'query',
      xmlns: 'http://jabber.org/protocol/disco#items'
      ignore_archived: ignore_archived
    @sendIQ stanza, (d)=>
      cb(d)
      if !d.error
        @store.set(key, iq: d)

  fetchRoom: (jid, cb = @noop)->
    jid = jid.split('/')[0]
    if @cachedRoomInfo[jid]
      cb @cachedRoomInfo[jid]
      return

    Strophe.info 'Getting room'
    stanza = $iq
      type: 'get'
      to: jid
    .c 'query',
      xmlns: 'http://jabber.org/protocol/disco#info'
    @sendIQ stanza, (data)=>
      @cachedRoomInfo[jid] = data unless data.error
      cb(data);

  fetchHistory: (jid, before, maxstanzas=50, id, cb = @noop)->
    Strophe.info 'Getting history'
    opts =
      xmlns: 'http://hipchat.com/protocol/history'
      maxstanzas: maxstanzas
      type: @getJidType(jid)

    if before?
      opts.before = @toXMPPDate(before)

    stanza = $iq
      type: 'get'
      to: jid,
      id: id
    .c 'query', opts
    @sendIQ stanza, cb

  fetchUserProfile: (jid, cb = @noop) ->
    return unless jid

    if @cached_profiles[jid] and cb
      cb(@cached_profiles[jid])
      return

    Strophe.info "Fetching profile for", jid

    getUserId = (jid) =>
      node = jid.split('@')[0]
      id = node.substr node.indexOf('_') + 1
      parseInt(id, 10) if id.match(/[0123456789]+/)

    user_id = getUserId(jid)
    opts =
      jid: jid
      profile_id: user_id
    stanza = $iq
      type: 'get'
      to: jid
    .c 'query',
      xmlns: 'http://hipchat.com/protocol/profile'

    @sendIQ stanza, (data)=>
      @cached_profiles[jid] = data unless data.error
      cb(data) if cb

  joinRoom: (room_jid, maxstanzas = 0, cb = @noop)=>
    pres = $pres
      type: 'available'
      to: room_jid + '/' + config.user_name
    .c 'x',
      xmlns: 'http://jabber.org/protocol/muc'
    .c 'history',
      maxstanzas: maxstanzas
    @once 'joinRoom', cb
    @connection.send pres.tree()

  joinChat: (jid, cb = @noop)=>
    @sendStateMessage jid, 'chat', 'active'

  joinAutoJoinRooms: (jidsArr, maxstanzas)=>
    _.each jidsArr || config.prefs.autoJoin, (room)=>
      @rooms_joined.push room
      if /@conf/.test(room.jid)
        @joinRoom room.jid
      else
        @joinChat room.jid

  sendMessage: (jid, message, id = null) ->
    attrs =
      to: jid
      type: @getJidType(jid)
      id: Math.random()*10000000|0
    if id != null then attrs['id'] = id
    node = $msg(attrs)
      .c('body')
      .t(message)
      .up()

    if attrs.type == 'chat'
      attrs.from = @current_user_jid
      node.c 'active',
        xmlns: 'http://jabber.org/protocol/chatstates'
      .up()
      .c 'x',
        xmlns: 'http://hipchat.com'
      .c 'echo'
    @connection.send(node.tree())

  sendEditMessage: (jid, text, original_mid, ts, id = null) ->
    attrs =
      to: jid
      type: @getJidType(jid)
      from: @current_user_jid
      id: Math.random()*10000000|0
    if id != null then attrs['id'] = id
    node = $msg(attrs)
      .c('body')
      .t(text)
      .up()

    node.c 'replace',
      xmlns: 'urn:xmpp:message-correct:0',
      id: original_mid,
      ts: ts
    .up()
    .c 'x',
      xmlns: 'http://hipchat.com'
    .c 'echo'
    @connection.send(node.tree())

  sendStateMessage: (jid, type, state) ->
    attrs =
      to: jid,
      type: type
      id: Math.random()*10000000|0
    node = $msg(attrs).c(state, {xmlns: 'http://jabber.org/protocol/chatstates'}).up();
    @connection.send(node.tree())

  createRoom: (name, topic, privacy, cb = @noop) ->
    to_slug = (str) ->
      str = str.replace(/^\s+|\s+$/g, "").toLowerCase() # trim and force lowercase
      from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;"
      to   = "aaaaeeeeiiiioooouuuunc------"
      for i in [i..from.length]
        str = str.replace(new RegExp(from.charAt(i), "g"), to.charAt(i))
      # remove accents, swap ñ for n, etc
      str = str.replace(/[^a-z0-9 -]/g, "").replace(/\s+/g, "-").replace(/-+/g, "-")
      # remove invalid chars, collapse whitespace and replace by -, collapse dashes
      return str # unnecessary line, but for clarity

    room_jid = config.group_id + '_' + to_slug(name) + '@' + config.conference_server
    pres = $pres
      type: 'available'
      to: room_jid + '/' + config.user_name
    .c 'x',
      xmlns: 'http://jabber.org/protocol/muc'
    .c 'history',
      maxstanzas: 50
    .up()
    .up()
    .c 'x',
      xmlns: 'http://jabber.org/protocol/muc#room'
    .c 'name'
    .t name
    .up()
    .c 'topic'
    .t topic
    .up()
    .c 'privacy'
    .t privacy
    @once 'createRoom', cb
    @connection.send pres.tree()

  renameRoom: (jid, new_name, cb = @noop)->
    Strophe.info 'Renaming room'
    stanza = $iq
      type: 'set'
      to: jid
    .c 'query',
      xmlns: 'http://hipchat.com'
    .c('rename')
    .t(new_name)
    @sendIQ stanza, cb

  deleteRoom: (jid, reason, cb = @noop)->
    Strophe.info 'Deleting room'
    stanza = $iq
      type: 'set'
      to: jid
    .c 'query',
      xmlns: 'http://jabber.org/protocol/muc#owner'
    .c('destroy')
    .c('reason')
    .t(reason)
    @sendIQ stanza, cb

  changeRoomPrivacy: (jid, new_privacy, cb = @noop)->
    Strophe.info 'Changing room privacy'
    stanza = $iq
      type: 'set'
      to: jid
    .c 'query',
      xmlns: 'http://hipchat.com'
    .c('privacy')
    .t(new_privacy)
    @sendIQ stanza, cb

  makeAPIRequest: (func, opts, cb = @noop) =>
    payload = $.extend opts,
      user_id: config.user_id
      group_id: config.group_id
      token: config.token.token
      format: 'json'
    $.ajax
      type: 'POST'
      dataType: 'json'
      url: @baseUrl + '/api/' + func
      data: payload
      async: true
      success: cb

  prefs_changed: false
  getBoolPref: (name, default_val) ->
    if name of config.prefs
      val = config.prefs[name]
      ret = false
      if val is "true" or val is true
        ret = true
      else if val is "false" or val is false
        ret = false
      else
        ret = val
      return ret
    default_val

  getPref: (name, default_val) ->
    return config.prefs[name]  if config.prefs[name]
    default_val

  savePreferences: ->
    if @prefs_changed
      @makeAPIRequest "save_preferences", # req token
        pref_data: config.prefs
      , null
      @prefs_changed = false
    return

  setPref: (name, val, save_now) ->
    config.prefs[name] = val
    @prefs_changed = true
    @savePreferences()  if save_now
    return

  sendUploadMessage: (data) ->
    if !@is_connected
      return false
    file_info = @x2js.xml2json(data.file_info)
    attrs =
      to: data.jid,
      type: data.type

    msg = $msg(attrs)
      .c('x', xmlns: 'http://hipchat.com/protocol/muc#room')
      .c('file', {'id': file_info.response.file_id}).up().up()


    if data.type != 'groupchat'
      msg.c('x', {xmlns: 'http://hipchat.com'})
      .c('echo').up().up()

    @connection.send(msg.tree());

  fetchFiles: (jid, before = null, after = null, limit = 50, cb = @noop) ->
    Strophe.info 'Requesting files'
    opts =
      xmlns: 'http://hipchat.com/protocol/files'
      limit: limit
    if before?
      opts.before = @toXMPPDate(before)
    stanza = $iq
      type: 'get'
      to: jid
    .c 'query', opts

    @sendIQ stanza, cb

  fetchLinks: (jid, before = null, after = null, limit = 50, cb = @noop) ->
    Strophe.info 'Requesting files'
    opts =
      xmlns: 'http://hipchat.com/protocol/links'
      limit: limit
    if before?
      opts.before = @toXMPPDate(before)
    stanza = $iq
      type: 'get'
      to: jid
    .c 'query', opts

    @sendIQ stanza, cb

  revokeOauth2Token: () ->
    opts =
      xmlns : "http://hipchat.com/protocol/auth"
      action : "delete"
    stanza = $iq
      type: 'set'
    .c 'query', opts
    .c 'type'
    .t 'oauth2'

    @sendIQ stanza, @noop

  setTopic: (jid, topic) ->
    attrs =
      to: jid
      type: 'groupchat'

    msg = $msg(attrs)
      .c('subject')
      .t(topic)

    @connection.send(msg.tree())

  setGuestAccess: (jid, enabled) ->
    val = if enabled then 1 else 0
    opts =
      xmlns: "http://hipchat.com"
    stanza = $iq
      to: jid
      type: 'set'
    .c 'query', opts
      .c 'guest_access'
      .t val

    @sendIQ stanza, @noop

  requestAddliveCredentials: (jid, cb = @noop) ->
    stanza = $iq
      to: jid
      type: 'get'
    .c 'query', xmlns: 'http://hipchat.com/protocol/addlive'

    done = (resp) ->
      cb(null, resp.query)

    fail = (resp) ->
      cb(resp.error);

    @sendIQ stanza, done, fail


  sendVideoMessage: (jid, type = 'call') ->
    if !@is_connected
      return false
    attrs =
      to: jid
      type: @getJidType(jid)
      from: @current_user_jid
      id: Math.random()*10000000|0

    node = $msg(attrs)
      .c 'x', xmlns: 'http://hipchat.com/protocol/addlive'
      .c type

    @connection.send(node.tree())

if typeof module != 'undefined'
  module.exports = HipChat
else
  window.HipChat = HipChat

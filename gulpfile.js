var gulp = require('gulp');
var gutil = require('gulp-util');
var concat = require('gulp-concat');
var coffee = require('gulp-coffee');
var uglify = require('gulp-uglify');

gulp.task('scripts', function() {
  gulp.src('./src/*.coffee')
    .pipe(coffee().on('error', gutil.log))
    .pipe(gulp.dest('./dist/'))
    .pipe(concat('hipchat.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./dist'));

  gulp.src('./node_modules/strophe/strophe.js')
    .pipe(concat('strophe.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./dist'));

  gulp.src('./node_modules/x2js/xml2json.js')
    .pipe(concat('xml2json.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./dist'));
});

gulp.task('watch', ['default'], function() {
  gulp.watch('./src/*.coffee', ['default']);
});

gulp.task('default', ['scripts']);
(function() {
  var HipChat, Storage,
    __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  Storage = (function() {
    function Storage(opts) {
      this.clear = __bind(this.clear, this);
      this.unset = __bind(this.unset, this);
      this.set = __bind(this.set, this);
      this.get = __bind(this.get, this);
      this.has = __bind(this.has, this);
      var e;
      this.prefix = 'hc.';
      this.expiry_prefix = '_';
      this.locations = ['local', 'session', 'global'];
      this.ttl = opts.ttl || 60 * 60 * 24 * 1000;
      this.disableStorage = opts.disableStorage;
      try {
        return 'localStorage' in window && window['localStorage'] === !null;
      } catch (_error) {
        e = _error;
        window.localStorage = window.sessionStorage;
      }
    }

    Storage.prototype.has = function(key) {
      if (this.disableStorage) {
        return;
      }
      return _.first(this.locations, (function(_this) {
        return function(location) {
          return _.keys(window[location + 'Storage']).indexOf(_this.prefix + key) !== -1;
        };
      })(this))[0];
    };

    Storage.prototype.get = function(key) {
      var e, expiry, location;
      if (this.disableStorage) {
        return;
      }
      if (key.indexOf(this.expiry_prefix) !== 0) {
        expiry = this.get(this.expiry_prefix + key);
        if ((new Date).getTime() > expiry) {
          this.unset(key);
          return;
        }
      }
      location = this.has(key);
      if (location) {
        try {
          return JSON.parse(window[location + 'Storage'].getItem(this.prefix + key).toString());
        } catch (_error) {
          e = _error;
          return;
        }
      }
    };

    Storage.prototype.set = function(key, val, location) {
      if (this.disableStorage) {
        return;
      }
      window[(location || 'local') + 'Storage'].setItem(this.prefix + key, JSON.stringify(val, function(k, v) {
        if (v instanceof RegExp) {
          return v.toString();
        }
        return v;
      }));
      if (key.indexOf(this.expiry_prefix) !== 0) {
        return this.set(this.expiry_prefix + key, (new Date).getTime() + this.ttl);
      }
    };

    Storage.prototype.unset = function(key) {
      var location;
      if (this.disableStorage) {
        return;
      }
      location = this.has(key);
      if (key.indexOf(this.expiry_prefix) !== 0) {
        this.unset(this.expiry_prefix + key);
      }
      if (location) {
        window[location + 'Storage'].removeItem(this.prefix + key);
        return true;
      }
      return false;
    };

    Storage.prototype.clear = function() {
      if (this.disableStorage) {
        return;
      }
      return _.each(this.locations, (function(_this) {
        return function(location) {
          return _.filter(_.keys(window[location + 'Storage']), function(key) {
            return key.match(_this.prefix);
          }).forEach(function(key) {
            return window[location + 'Storage'].removeItem(key);
          });
        };
      })(this));
    };

    return Storage;

  })();

  HipChat = (function() {
    function HipChat(opts) {
      this.makeAPIRequest = __bind(this.makeAPIRequest, this);
      this.joinAutoJoinRooms = __bind(this.joinAutoJoinRooms, this);
      this.joinChat = __bind(this.joinChat, this);
      this.joinRoom = __bind(this.joinRoom, this);
      this.once = __bind(this.once, this);
      this.on = __bind(this.on, this);
      this.onConnectionFail = __bind(this.onConnectionFail, this);
      this.disconnect = __bind(this.disconnect, this);
      this.onDisconnect = __bind(this.onDisconnect, this);
      this.onConnect = __bind(this.onConnect, this);
      this.connect = __bind(this.connect, this);
      this.keepAlive = __bind(this.keepAlive, this);
      this.checkSession = __bind(this.checkSession, this);
      this.isXMPPSessionExpired = __bind(this.isXMPPSessionExpired, this);
      this.isXMPPActive = __bind(this.isXMPPActive, this);
      this.onUserAction = __bind(this.onUserAction, this);
      this.onUserIdle = __bind(this.onUserIdle, this);
      this.attachXmlListener = __bind(this.attachXmlListener, this);
      this._fixXhtmlMessageBody = __bind(this._fixXhtmlMessageBody, this);
      this.onConnectChange = __bind(this.onConnectChange, this);
      var log_prefix;
      this.baseUrl = opts.baseUrl || "";
      this.clientType = opts.clientType ? '/' + opts.clientType : '';
      this.clientVersion = opts.clientVersion || 1;
      this.is_initial_connect = true;
      this.do_reconnect = true;
      this.KEEP_ALIVE_MS = 25000;
      this.HEARTBEAT_MS = 30 * 1000;
      this.RECONNECT_BACKOFF_FACTOR = 3;
      this.RECONNECT_DELAY_MS = 2 * 1000;
      this.RECONNECT_MAX_DELAY = 60 * 1000;
      this.MAX_RECONNECT_TIME = 10 * 60 * 1000;
      this.CACHE_TTL = 90 * 24 * 60 * 60 * 1000;
      this.reconnect_delay = this.RECONNECT_DELAY_MS;
      this.reconnect_attempt_number = 0;
      this.reconnect_time = 0;
      this.rooms_joined = [];
      this.cached_profiles = {};
      this.cachedRoomInfo = {};
      this.token_info = {};
      this.noop = function() {
        return {};
      };
      this.onInitialConnectCallback = opts.onInitialConnect || this.noop;
      this.onConnectionChangeCallback = opts.onConnectionChange || this.noop;
      this.onConnectionFailCallback = opts.onConnectionFail || this.noop;
      this.onConnectCallback = opts.onConnect || this.noop;
      this.onServerDataCallback = opts.onServerData || this.noop;
      this.onDisconnectCallback = opts.onDisconnect || this.noop;
      this.onReconnectingCallback = opts.onReconnecting || this.noop;
      this.onReconnectionSuccessCallback = opts.onReconnectionSuccess || this.noop;
      this.onReconnectionFailCallback = opts.onReconnectionFail || this.noop;
      this.onUserIdleCallback = opts.onUserIdle || this.noop;
      this.onUserActiveCallback = opts.onUserActive || this.noop;
      this.onStaleSessionCallback = opts.onStaleSession || this.noop;
      this.onMaxReconnectAttemts = opts.onMaxReconnectAttemts || this.noop;
      this.disableStorage = opts.disableStorage || false;
      this.store = new Storage({
        ttl: this.CACHE_TTL,
        disableStorage: this.disableStorage
      });
      this.previous_presence = this.current_presence = {
        show: 'chat',
        status: null,
        type: 'available'
      };
      log_prefix = '[HipChatJS]';
      Strophe.log = function(level, msg) {
        if (typeof window !== 'undefined' && 'console' in window && window.HC_LOG) {
          switch (level) {
            case Strophe.LogLevel.DEBUG:
              return console.debug(log_prefix, msg);
            case Strophe.LogLevel.INFO:
              return console.info(log_prefix, msg);
            case Strophe.LogLevel.WARN:
              return console.warn(log_prefix, msg);
            case Strophe.LogLevel.ERROR:
            case Strophe.LogLevel.FATAL:
              return console.error(log_prefix, msg);
          }
        }
      };
      this.x2js = new X2JS;
      this.init = (function(_this) {
        return function(data) {
          if (data.error) {
            window.location.reload();
          }
          window.config = data;
          if (_this.store.get('uid') !== config.user_id) {
            _this.store.clear();
          }
          _this.store.set('uid', config.user_id);
          _this.config = data;
          if (data.prefs.isIdleTimeEnabled) {
            if (!data.prefs.secondsToIdle || data.prefs.secondsToIdle && data.prefs.secondsToIdle < 900) {
              data.prefs.secondsToIdle = 900;
            }
            _this.IDLE_DELAY_MS = data.prefs.secondsToIdle * 1000;
          } else {
            _this.IDLE_DELAY_MS = 60 * 60 * 24 * 365 * 1000;
          }
          _this.setupConnection();
          _this.connect();
          _this.setupIdleEvents();
          return $(window).on('beforeunload', function() {
            if (_this.is_connected) {
              return _this.disconnect(true);
            }
          });
        };
      })(this);
      this.parsedCallback = (function(_this) {
        return function(data, once, cb) {
          if (typeof once === 'function') {
            cb = once;
            once = false;
          }
          cb(_this.x2js.xml2json(data));
          return !once;
        };
      })(this);
      this.clearData = function() {
        this.room_rosters = [];
        this.room_history_loaded = {};
        this.room_loaded = {};
        this.roster = [];
        return this.roster_loaded = false;
      };
      this.randomBetween = (function(_this) {
        return function(min, max) {
          return Math.floor(Math.random() * (max - min + 1)) + min;
        };
      })(this);
      this.decorrelatedJitter = (function(_this) {
        return function(cap, base, sleep, backoff_factor) {
          return Math.min(cap, _this.randomBetween(base, sleep * backoff_factor));
        };
      })(this);
      this.refreshReconnectTimer = (function(_this) {
        return function() {
          if (_this.is_connected) {
            return _this.reconnect_timer = null;
          } else if (_this.do_reconnect) {
            return _this.reconnect();
          }
        };
      })(this);
      this.scheduleReconnectAttempt = function() {
        this.reconnect_delay = this.decorrelatedJitter(this.RECONNECT_MAX_DELAY, this.RECONNECT_DELAY_MS, this.reconnect_delay, this.RECONNECT_BACKOFF_FACTOR);
        return this.reconnect_timer = setTimeout(_.bind(this.refreshReconnectTimer), this.reconnect_delay);
      };
      this.sendIQ = (function(_this) {
        return function(stanza, cb) {
          return _this.connection.sendIQ(stanza.tree(), function(data) {
            return _this.parsedCallback(data, cb);
          }, function(err) {
            return _this.parsedCallback(err, cb);
          });
        };
      })(this);
      this.subscribe = (function(_this) {
        return function(evt, once, opts, cb) {
          var from, id, ns, type;
          if (once == null) {
            once = false;
          }
          if (opts == null) {
            opts = null;
          }
          if (evt === 'roomChange') {
            evt = 'iq';
            type = 'set';
            ns = 'jabber:client';
          } else if (evt === 'joinRoom' || evt === 'createRoom') {
            evt = 'presence';
            type = null;
            ns = null;
          } else {
            type = null;
            ns = null;
          }
          if (opts) {
            id = opts.id;
            from = opts.from;
          }
          return _this.connection.addHandler(function(data) {
            return _this.parsedCallback(data, once, cb);
          }, ns, evt, type, id, from);
        };
      })(this);
      this.toXMPPDate = function(dttm) {
        if (typeof dttm === 'string') {
          return dttm;
        }
        return (new Date(dttm.setMilliseconds(0))).toISOString().replace(/\.000/, '');
      };
      this.getJidType = function(jid) {
        var re;
        re = new RegExp(config.conference_server);
        if (re.test(jid)) {
          return 'groupchat';
        } else {
          return 'chat';
        }
      };
      this.getJidFromNickname = (function(_this) {
        return function(nickname) {
          var rslts;
          if (/\//.test(nickname)) {
            nickname = nickname.split('/')[1];
          }
          rslts = _.where(_this.cached_profiles, {
            query: {
              name: nickname
            }
          });
          return rslts[0];
        };
      })(this);
      this.callIfCached = function(key, cb) {
        var cached_val;
        cached_val = this.store.get(key);
        if (cached_val) {
          cb(cached_val);
          this.xmlListenerCallback(cached_val);
        }
        return cached_val;
      };
      this.monkeypatchStropheForHipchatAuth = function(successCallback) {
        var clientType, clientVersion, originalXHRFactory;
        clientType = this.clientType;
        clientVersion = this.clientVersion;
        Strophe.Connection.prototype.authenticate = function(matched) {
          var request_auth_exchange;
          this._sasl_success_handler = this._addSysHandler(successCallback.bind(this), null, "success", null, null);
          this._sasl_failure_handler = this._addSysHandler(this._sasl_failure_cb.bind(this), null, "failure", null, null);
          this._sasl_challenge_handler = this._addSysHandler(this._sasl_challenge_cb.bind(this), null, "challenge", null, null);
          if (config.auth_method === 'nonce') {
            request_auth_exchange = $build("auth", {
              xmlns: Strophe.NS.SASL,
              mechanism: 'X-HIPCHAT-WEB',
              nonce: config.auth_nonce,
              oauth2_token: 'true',
              node: 'http://hipchat.com/client/web' + clientType,
              ver: clientVersion
            });
            request_auth_exchange.t(config.user_jid.split('@')[0]);
          } else if (config.auth_method === 'oauth2') {
            request_auth_exchange = $build("auth", {
              xmlns: 'http://hipchat.com',
              mechanism: 'oauth2',
              oauth2_token: 'true',
              node: 'http://hipchat.com/client/web' + clientType,
              ver: clientVersion
            });
            request_auth_exchange.t(Base64.encode('\u0000' + config.auth_token + '\u0000web'));
          } else if (config.auth_method === 'password') {
            request_auth_exchange = $build("auth", {
              xmlns: 'http://hipchat.com',
              oauth2_token: 'true',
              node: 'http://hipchat.com/client/web' + clientType,
              ver: clientVersion
            });
            request_auth_exchange.t(Base64.encode('\u0000' + config.auth_email + '\u0000' + config.auth_password + '\u0000web'));
          }
          return this.send(request_auth_exchange.tree());
        };
        Strophe.Connection.prototype._sasl_auth1_cb = function(elem) {
          this.authenticated = true;
          return this._changeConnectStatus(Strophe.Status.CONNECTED, null);
        };
        originalXHRFactory = Strophe.Request.prototype._newXHR;
        return Strophe.Request.prototype._newXHR = function() {
          var xhr;
          xhr = (originalXHRFactory.bind(this))();
          xhr.withCredentials = true;
          return xhr;
        };
      };
      this.hipchatAuthSuccessCallback = function(elem) {
        var oldRestartMethod, returnVal;
        config.oauth2_token = elem.getAttribute('oauth2_token');
        this.jid = elem.textContent;
        oldRestartMethod = this._sendRestart;
        if (config.auth_method !== 'nonce') {
          this._sendRestart = function() {
            return {};
          };
        }
        returnVal = this._sasl_success_cb(elem);
        this._sendRestart = oldRestartMethod;
        if (config.auth_method !== 'nonce') {
          this._sasl_auth1_cb(null);
        }
        return returnVal;
      };
      if (opts.initState && !this.baseUrl) {
        this.init(opts.initState);
      } else {
        $.ajax({
          url: this.baseUrl + '/chat/session?nonce_auth=1',
          dataType: 'json',
          success: this.init
        });
      }
    }

    HipChat.prototype.onConnectChange = function(status, condition) {
      if (status === Strophe.Status.CONNECTING) {
        this.connection_failed = false;
        Strophe.debug('[Strophe] is connecting]');
      } else if (status === Strophe.Status.CONNFAIL) {
        Strophe.debug('[Strophe] Offline. Condition: ' + condition);
        this.connection_failed = true;
        this.onConnectionFail(condition);
      } else if (status === Strophe.Status.DISCONNECTING) {
        Strophe.debug('[Strophe] is disconnecting. Condition: ' + condition);
      } else if (status === Strophe.Status.DISCONNECTED) {
        Strophe.debug('[Strophe] is disconnected.');
        this.onDisconnect();
      } else if (status === Strophe.Status.CONNECTED) {
        Strophe.debug('[Strophe] is connected.');
        this.connection_failed = false;
        this.onConnect();
      } else if (status === Strophe.Status.ATTACHED) {
        Strophe.debug('[Strophe] is attached.');
        this.connection_failed = false;
      } else if (status === Strophe.Status.ERROR) {
        Strophe.error('[Strophe] encountered error: ' + condition);
      }
      return this.onConnectionChangeCallback(status, condition);
    };

    HipChat.prototype._fixXhtmlMessageBody = function(message, raw) {
      var matches, messageExtractor, rawMessage, xhtmlBodyExtractor, _ref, _ref1;
      messageExtractor = new RegExp("mid=['\"]" + message.mid + "['\"]\.+?</message>", "i");
      xhtmlBodyExtractor = new RegExp("<body[^>]*xmlns=[\"']http:\\/\\/www\\.w3\\.org\\/1999\\/xhtml[\"'][^>]*>(.*?)<\/body>", "i");
      if ((message != null ? (_ref = message.html) != null ? (_ref1 = _ref.body) != null ? _ref1.xmlns : void 0 : void 0 : void 0) === 'http://www.w3.org/1999/xhtml') {
        matches = messageExtractor.exec(raw);
        if (matches != null ? matches.length : void 0) {
          rawMessage = matches[0];
          matches = xhtmlBodyExtractor.exec(rawMessage);
          if ((matches != null ? matches.length : void 0) > 1) {
            return message.html.body.__text = matches[1];
          }
        }
      }
    };

    HipChat.prototype.attachXmlListener = function(cb) {
      var json;
      if (cb == null) {
        cb = this.noop;
      }
      this.xmlListenerCallback = cb;
      json = null;
      this.connection.xmlInput = (function(_this) {
        return function(elem) {
          _this.last_xmpp_activity = (new Date).getTime();
          return json = _this.x2js.xml2json(elem);
        };
      })(this);
      return this.connection.rawInput = (function(_this) {
        return function(raw) {
          var messageArray, messageObj, _i, _len;
          if (json != null ? json.message : void 0) {
            messageArray = Array.isArray(json.message) ? json.message : [json.message];
            for (_i = 0, _len = messageArray.length; _i < _len; _i++) {
              messageObj = messageArray[_i];
              _this._fixXhtmlMessageBody(messageObj, raw);
            }
          }
          return cb(json);
        };
      })(this);
    };

    HipChat.prototype.onUserIdle = function() {
      if (!this.is_connected) {
        return;
      }
      Strophe.info('Going idle');
      this.is_idle = true;
      this.setPresence('away', this.current_presence.status, this.IDLE_DELAY_MS / 1000);
      return this.onUserIdleCallback(this);
    };

    HipChat.prototype.onUserActive = function() {
      if (!this.is_connected) {
        return;
      }
      Strophe.info('Returning from idle', this.previous_presence, this.current_presence);
      this.is_idle = false;
      this.setPresence(this.previous_presence.show, this.previous_presence.status);
      return this.onUserActiveCallback(this);
    };

    HipChat.prototype.onUserAction = function(event) {
      clearTimeout(this.idle_timeout);
      if (this.is_idle) {
        this.onUserActive();
      }
      this.idle_timeout = setTimeout(_.bind(this.onUserIdle), this.IDLE_DELAY_MS);
      return this.throttledKeepAlive();
    };

    HipChat.prototype.setupIdleEvents = function() {
      $(document).on('mousemove.idle keydown.idle DOMMouseScroll.idle mousewheel.idle mousedown.idle', _.bind(this.onUserAction));
      this.idle_timeout = setTimeout(_.bind(this.onUserIdle), this.IDLE_DELAY_MS);
      this.check_session_timer = setInterval(_.bind(this.checkSession), this.HEARTBEAT_MS);
      return this.throttledKeepAlive = _.throttle((function(_this) {
        return function() {
          if (_this.isXMPPActive()) {
            return;
          }
          return _this.keepAlive('userAction');
        };
      })(this), this.HEARTBEAT_MS);
    };

    HipChat.prototype.isXMPPActive = function() {
      if (!this.last_xmpp_activity) {
        return false;
      }
      return ((new Date).getTime() - this.last_xmpp_activity) > this.HEARTBEAT_MS;
    };

    HipChat.prototype.isXMPPSessionExpired = function() {
      var age;
      if (!this.last_xmpp_activity) {
        return false;
      }
      age = (new Date).getTime() - this.last_xmpp_activity;
      if (age > this.KEEP_ALIVE_MS) {
        return age;
      } else {
        return false;
      }
    };

    HipChat.prototype.checkSession = function() {
      if (this.session_age = this.isXMPPSessionExpired()) {
        Strophe.debug('[Strophe] Stale session detected. Session age: ' + this.session_age);
        this.onStaleSessionCallback(this.session_age);
        return this.keepAlive('checkSession');
      }
    };

    HipChat.prototype.updateIdleTime = function(newIdleTime) {
      clearTimeout(this.idle_timeout);
      clearInterval(this.check_session_timer);
      this.idle_timeout = null;
      $(document).off('.idle');
      this.IDLE_DELAY_MS = newIdleTime * 1000;
      return this.setupIdleEvents();
    };

    HipChat.prototype.keepAlive = function(origin) {
      var ping;
      if (!this.is_connected) {
        return;
      }
      Strophe.debug("[Strophe] Stayin' alive! Called from " + origin);
      ping = $iq({
        type: 'get',
        to: config.chat_server
      }).c('ping', {
        xmlns: 'urn:xmpp:ping'
      });
      if (this.connection) {
        this.connection.send(ping);
      } else {
        return;
      }
      return true;
    };

    HipChat.prototype.setupConnection = function() {
      if (this.config.auth_method) {
        this.monkeypatchStropheForHipchatAuth(this.hipchatAuthSuccessCallback);
      }
      if (this.connection) {
        this.connection.disconnect('resetting before reconnect');
        return this.connection.reset();
      } else {
        return this.connection = new Strophe.Connection(config.bind_url);
      }
    };

    HipChat.prototype.connect = function(isReconnection) {
      var connectChangeWrapper;
      if (isReconnection == null) {
        isReconnection = false;
      }
      connectChangeWrapper = (function(_this) {
        return function(status, condition) {
          if (isReconnection) {
            if (status === Strophe.Status.CONNECTED) {
              _this.onReconnectionSuccessCallback(true);
            }
            if (status === Strophe.Status.AUTHFAIL || status === Strophe.Status.CONNFAIL) {
              _this.onReconnectionFailCallback(data);
            }
          }
          return _this.onConnectChange(status, condition);
        };
      })(this);
      if (this.config.auth_method) {
        Strophe.debug('[Strophe] connect()');
        if (!this.is_connected) {
          return this.connection.connect(config.jid, null, connectChangeWrapper, null, null, config.route);
        }
      } else {
        Strophe.debug('[Strophe] attach()');
        if (!this.is_connected) {
          this.connection.attach(config.jid, config.sid, config.rid, connectChangeWrapper);
        }
        return this.onConnect();
      }
    };

    HipChat.prototype.onConnect = function() {
      this.do_reconnect = true;
      this.is_connected = true;
      clearTimeout(this.reconnect_timer);
      this.reconnect_timer = null;
      this.reconnect_delay = this.RECONNECT_DELAY_MS;
      this.reconnect_attempt_number = 0;
      this.reconnect_time = 0;
      this.current_user_jid = config.user_jid;
      this.current_user_nickname_jid = config.user_name + '@' + config.chat_server;
      this.attachXmlListener(this.onServerDataCallback);
      this.setPresence(null, null, null, true, true);
      if (this.is_initial_connect) {
        this.fetchInitData();
        this.onInitialConnectCallback(this);
      }
      this.is_initial_connect = false;
      return this.onConnectCallback(this);
    };

    HipChat.prototype.onDisconnect = function() {
      this.is_connected = false;
      if (!this.reconnect_timer) {
        this.refreshReconnectTimer();
      }
      return this.onDisconnectCallback(this);
    };

    HipChat.prototype.disconnect = function(should_not_reconnect) {
      Strophe.debug('[Strophe] disconnect()');
      if (should_not_reconnect) {
        this.do_reconnect = false;
      }
      this.clearData();
      if (this.connection) {
        this.connection.disconnect();
      }
      if (this.timed_handler && this.connection) {
        this.connection.deleteTimedHandler(this.timed_handler);
      }
      this.connection = null;
      clearTimeout(this.auth_token_refresh_timer);
      return this.auth_token_refresh_timer = null;
    };

    HipChat.prototype.onConnectionFail = function(condition) {
      this.is_connected = false;
      switch (condition) {
        case 'conflict':
          this.do_reconnect = false;
          break;
        case 'not-allowed':
          location.reload(true);
          break;
        case 'plan-change':
          break;
        case 'system-shutdown':
          break;
        case 'see-other-host':
          break;
      }
      return this.onConnectionFailCallback(condition);
    };

    HipChat.prototype.reconnect = function(cb) {
      var errCb, errHandler, opts, successCb;
      if (cb == null) {
        cb = this.noop;
      }
      if (this.is_connected) {
        return;
      }
      Strophe.debug('[Strophe] reconnect(). Next attempt in ' + this.reconnect_delay / 1000 + 's');
      this.onReconnectingCallback(this.reconnect_attempt_number, this.reconnect_delay);
      if (this.reconnect_time >= this.MAX_RECONNECT_TIME) {
        this.onMaxReconnectAttemts(this);
        return;
      }
      this.reconnect_attempt_number++;
      this.reconnect_time += this.reconnect_delay;
      opts = {
        is_guest: config.is_guest,
        guest_key: config.guest_key,
        uid: config.user_id,
        nonce_auth: config.auth_method === 'nonce' ? 1 : 0
      };
      errHandler = (function(_this) {
        return function(data) {
          _this.onReconnectionFailCallback(data);
          cb('fail');
          if (data.refresh_page === true) {
            return window.location.reload();
          }
        };
      })(this);
      successCb = (function(_this) {
        return function(data) {
          if (data && !data.error) {
            window.config = data;
            _this.config = data;
            _this.clearData();
            _this.setupConnection();
            _this.connect(true);
            cb('success');
          } else if (data && data.error) {
            errHandler(data);
          }
          return _this.scheduleReconnectAttempt();
        };
      })(this);
      errCb = (function(_this) {
        return function(jqXHR, textStatus, errorThrown) {
          errHandler(jqXHR);
          Strophe.error('Error trying to reconnect. Status: ' + textStatus + ' -- error: ' + errorThrown);
          if (typeof navigator === 'undefined' || navigator.onLine) {
            _this.onConnectionChangeCallback(Strophe.Status.DISCONNECTED);
            return _this.do_reconnect = false;
          } else {
            return _this.scheduleReconnectAttempt();
          }
        };
      })(this);
      return $.ajax({
        url: this.baseUrl + '/chat/session',
        dataType: 'json',
        data: opts,
        success: successCb,
        error: errCb
      });
    };

    HipChat.prototype.on = function(evt, opts, cb) {
      if (cb == null) {
        cb = this.noop;
      }
      if (typeof opts === 'function') {
        cb = opts;
        opts = null;
      }
      return this.subscribe(evt, false, opts, cb);
    };

    HipChat.prototype.once = function(evt, opts, cb) {
      if (cb == null) {
        cb = this.noop;
      }
      if (typeof opts === 'function') {
        cb = opts;
        opts = null;
      }
      return this.subscribe(evt, true, opts, cb);
    };

    HipChat.prototype.off = function(subscriptionRef) {
      return this.connection.deleteHandler(subscriptionRef);
    };

    HipChat.prototype.setPresence = function(show, status, idle, send_caps, roster_presences) {
      var stanza;
      if (roster_presences == null) {
        roster_presences = false;
      }
      if (!this.is_connected) {
        return;
      }
      show = show || 'chat';
      Strophe.info('Setting presence: ' + ' - ' + show + ' - ' + idle);
      stanza = $pres({
        roster_presences: roster_presences.toString()
      });
      if (show && show !== 'chat') {
        stanza.c('show').t(show).up();
      }
      if (typeof status === 'string') {
        stanza.c('status').t(status).up();
      }
      if (idle) {
        stanza.c('query', {
          xmlns: 'jabber:iq:last',
          seconds: idle
        }).up();
      }
      if (send_caps) {
        stanza.c('c', {
          xmlns: 'http://jabber.org/protocol/caps',
          node: 'http://hipchat.com/client/web' + this.clientType,
          ver: this.clientVersion
        }).up();
      }
      this.connection.send(stanza.tree());
      this.previous_presence = this.current_presence;
      return this.current_presence = {
        type: 'available',
        show: show,
        status: status,
        idle: idle,
        send_caps: send_caps
      };
    };

    HipChat.prototype.unsubscribeFromPresences = function() {
      var stanza;
      stanza = $pres({
        roster_presences: 'false'
      });
      return this.connection.send(stanza.tree());
    };

    HipChat.prototype.leaveRoom = function(jid, type, message) {
      var stanza;
      if (type === 'chat') {
        return this.sendStateMessage(jid, type, 'gone');
      }
      stanza = $pres({
        to: jid + '/' + config.user_name,
        type: 'unavailable'
      });
      if (message) {
        stanza.c('status').t(message);
      }
      return this.connection.send(stanza.tree());
    };

    HipChat.prototype.fetchAPIv1AuthToken = function(cb) {
      var dt, stanza;
      if (cb == null) {
        cb = this.noop;
      }
      dt = new Date;
      if (this.token_info && this.token_info.expiration > dt.getTime()) {
        this.token_info.token;
      }
      stanza = $iq({
        type: 'get',
        to: config.chat_server
      }).c('query', {
        xmlns: 'http://hipchat.com/protocol/auth'
      });
      return this.sendIQ(stanza, (function(_this) {
        return function(d) {
          _this.token_info = {
            token: d.query.token['__text'],
            expiration: parseInt(d.query.token.expiration, 10) * 1000
          };
          _this.auth_token_refresh_timer = setTimeout(_this.fetchAPIv1AuthToken.bind(_this), (_this.token_info.expiration - dt.getTime()) + 5000);
          return cb(_this.token_info);
        };
      })(this));
    };

    HipChat.prototype.fetchInitData = function(cb) {
      var stanza;
      if (cb == null) {
        cb = this.noop;
      }
      stanza = $iq({
        type: 'get',
        id: Math.random() * 10000000 | 0
      }).c('query', {
        xmlns: 'http://hipchat.com/protocol/startup',
        send_auto_join_user_presences: true
      });
      return this.sendIQ(stanza, (function(_this) {
        return function(d) {
          var dt;
          dt = new Date;
          _this.token_info = {
            token: d.query.token['__text'],
            expiration: parseInt(d.query.token.expiration, 10) * 1000
          };
          _this.auth_token_refresh_timer = setTimeout(_this.fetchAPIv1AuthToken.bind(_this), (_this.token_info.expiration - dt.getTime()) + 5000);
          return cb(d);
        };
      })(this));
    };

    HipChat.prototype.fetchEmoticons = function(cb) {
      var cached_val, stanza;
      if (cb == null) {
        cb = this.noop;
      }
      cached_val = this.callIfCached('emoticons', cb);
      stanza = $iq({
        type: 'get'
      }).c('query', {
        xmlns: 'http://hipchat.com/protocol/emoticons',
        ver: cached_val && cached_val.query ? cached_val.query.ver : ''
      });
      return this.sendIQ(stanza, (function(_this) {
        return function(d) {
          if (d.query) {
            cb(d);
            if (!d.error) {
              return _this.store.set('emoticons', {
                iq: d
              });
            }
          }
        };
      })(this));
    };

    HipChat.prototype.fetchRoster = function(cb, cbCached) {
      var cached_val, stanza;
      if (cb == null) {
        cb = this.noop;
      }
      cached_val = this.callIfCached('roster', cb);
      stanza = $iq({
        type: 'get'
      }).c('query', {
        xmlns: 'jabber:iq:roster',
        ver: cached_val && cached_val.query ? cached_val.query.ver : ''
      });
      return this.sendIQ(stanza, (function(_this) {
        return function(d) {
          if (d.query) {
            cb(d);
            if (!d.error) {
              return _this.store.set('roster', {
                iq: d
              });
            }
          }
        };
      })(this));
    };

    HipChat.prototype.fetchPresence = function(uid, cb) {
      var stanza;
      if (uid == null) {
        uid = [];
      }
      if (cb == null) {
        cb = this.noop;
      }
      if (!Array.isArray(uid)) {
        uid = [uid];
      }
      stanza = $iq({
        type: 'get'
      }).c('query', {
        xmlns: 'http://hipchat.com/protocol/presence'
      });
      uid.forEach(function(id) {
        return this.c('uid', id).up();
      }, stanza);
      return this.sendIQ(stanza, cb);
    };

    HipChat.prototype.limitPresencesToUsers = function(uid, id, cb) {
      var stanza;
      if (uid == null) {
        uid = [];
      }
      if (id == null) {
        id = _.uniqueId();
      }
      if (cb == null) {
        cb = this.noop;
      }
      if (!Array.isArray(uid)) {
        uid = [uid];
      }
      stanza = $iq({
        type: 'set',
        id: id
      }).c('query', {
        xmlns: 'http://hipchat.com/protocol/presence',
        action: 'presence_filter'
      });
      uid.forEach(function(id) {
        return this.c('uid', id).up();
      }, stanza);
      return this.sendIQ(stanza, cb);
    };

    HipChat.prototype.inviteUsersToRoom = function(room_jid, user_jids, reason, id) {
      var attrs, node;
      if (user_jids == null) {
        user_jids = [];
      }
      if (reason == null) {
        reason = '';
      }
      if (id == null) {
        id = null;
      }
      if (!Array.isArray(user_jids)) {
        user_jids = [user_jids];
      }
      attrs = {
        to: room_jid,
        id: Math.random() * 10000000 | 0
      };
      if (id !== null) {
        attrs['id'] = id;
      }
      node = $msg(attrs).c('x', {
        xmlns: 'http://jabber.org/protocol/muc#user'
      });
      user_jids.forEach(function(jid) {
        this.c('invite', {
          to: jid
        });
        this.c('reason');
        this.t(reason);
        this.up();
        return this.up();
      }, node);
      return this.connection.send(node.tree());
    };

    HipChat.prototype.removeUsersFromRoom = function(room_jid, user_jids, cb) {
      var stanza;
      if (user_jids == null) {
        user_jids = [];
      }
      if (cb == null) {
        cb = this.noop;
      }
      if (!Array.isArray(user_jids)) {
        user_jids = [user_jids];
      }
      Strophe.info('Removing users from room');
      stanza = $iq({
        type: 'set',
        to: room_jid
      }).c('query', {
        xmlns: 'http://hipchat.com'
      });
      user_jids.forEach(function(jid) {
        this.c('item', {
          jid: jid,
          affiliation: "none"
        });
        return this.up();
      }, stanza);
      return this.sendIQ(stanza, cb);
    };

    HipChat.prototype.fetchRooms = function(ignore_archived, cb) {
      var key, stanza;
      if (ignore_archived == null) {
        ignore_archived = true;
      }
      if (cb == null) {
        cb = this.noop;
      }
      Strophe.info('Getting room list');
      key = !ignore_archived ? 'rooms-w-archived' : 'rooms-wo-archived';
      this.callIfCached(key, cb);
      stanza = $iq({
        type: 'get',
        to: config.conference_server
      }).c('query', {
        xmlns: 'http://jabber.org/protocol/disco#items',
        ignore_archived: ignore_archived
      });
      return this.sendIQ(stanza, (function(_this) {
        return function(d) {
          cb(d);
          if (!d.error) {
            return _this.store.set(key, {
              iq: d
            });
          }
        };
      })(this));
    };

    HipChat.prototype.fetchRoom = function(jid, cb) {
      var stanza;
      if (cb == null) {
        cb = this.noop;
      }
      jid = jid.split('/')[0];
      if (this.cachedRoomInfo[jid]) {
        cb(this.cachedRoomInfo[jid]);
        return;
      }
      Strophe.info('Getting room');
      stanza = $iq({
        type: 'get',
        to: jid
      }).c('query', {
        xmlns: 'http://jabber.org/protocol/disco#info'
      });
      return this.sendIQ(stanza, (function(_this) {
        return function(data) {
          if (!data.error) {
            _this.cachedRoomInfo[jid] = data;
          }
          return cb(data);
        };
      })(this));
    };

    HipChat.prototype.fetchHistory = function(jid, before, maxstanzas, id, cb) {
      var opts, stanza;
      if (maxstanzas == null) {
        maxstanzas = 50;
      }
      if (cb == null) {
        cb = this.noop;
      }
      Strophe.info('Getting history');
      opts = {
        xmlns: 'http://hipchat.com/protocol/history',
        maxstanzas: maxstanzas,
        type: this.getJidType(jid)
      };
      if (before != null) {
        opts.before = this.toXMPPDate(before);
      }
      stanza = $iq({
        type: 'get',
        to: jid,
        id: id
      }).c('query', opts);
      return this.sendIQ(stanza, cb);
    };

    HipChat.prototype.fetchUserProfile = function(jid, cb) {
      var getUserId, opts, stanza, user_id;
      if (cb == null) {
        cb = this.noop;
      }
      if (!jid) {
        return;
      }
      if (this.cached_profiles[jid] && cb) {
        cb(this.cached_profiles[jid]);
        return;
      }
      Strophe.info("Fetching profile for", jid);
      getUserId = (function(_this) {
        return function(jid) {
          var id, node;
          node = jid.split('@')[0];
          id = node.substr(node.indexOf('_') + 1);
          if (id.match(/[0123456789]+/)) {
            return parseInt(id, 10);
          }
        };
      })(this);
      user_id = getUserId(jid);
      opts = {
        jid: jid,
        profile_id: user_id
      };
      stanza = $iq({
        type: 'get',
        to: jid
      }).c('query', {
        xmlns: 'http://hipchat.com/protocol/profile'
      });
      return this.sendIQ(stanza, (function(_this) {
        return function(data) {
          if (!data.error) {
            _this.cached_profiles[jid] = data;
          }
          if (cb) {
            return cb(data);
          }
        };
      })(this));
    };

    HipChat.prototype.joinRoom = function(room_jid, maxstanzas, cb) {
      var pres;
      if (maxstanzas == null) {
        maxstanzas = 0;
      }
      if (cb == null) {
        cb = this.noop;
      }
      pres = $pres({
        type: 'available',
        to: room_jid + '/' + config.user_name
      }).c('x', {
        xmlns: 'http://jabber.org/protocol/muc'
      }).c('history', {
        maxstanzas: maxstanzas
      });
      this.once('joinRoom', cb);
      return this.connection.send(pres.tree());
    };

    HipChat.prototype.joinChat = function(jid, cb) {
      if (cb == null) {
        cb = this.noop;
      }
      return this.sendStateMessage(jid, 'chat', 'active');
    };

    HipChat.prototype.joinAutoJoinRooms = function(jidsArr, maxstanzas) {
      return _.each(jidsArr || config.prefs.autoJoin, (function(_this) {
        return function(room) {
          _this.rooms_joined.push(room);
          if (/@conf/.test(room.jid)) {
            return _this.joinRoom(room.jid);
          } else {
            return _this.joinChat(room.jid);
          }
        };
      })(this));
    };

    HipChat.prototype.sendMessage = function(jid, message, id) {
      var attrs, node;
      if (id == null) {
        id = null;
      }
      attrs = {
        to: jid,
        type: this.getJidType(jid),
        id: Math.random() * 10000000 | 0
      };
      if (id !== null) {
        attrs['id'] = id;
      }
      node = $msg(attrs).c('body').t(message).up();
      if (attrs.type === 'chat') {
        attrs.from = this.current_user_jid;
        node.c('active', {
          xmlns: 'http://jabber.org/protocol/chatstates'
        }).up().c('x', {
          xmlns: 'http://hipchat.com'
        }).c('echo');
      }
      return this.connection.send(node.tree());
    };

    HipChat.prototype.sendEditMessage = function(jid, text, original_mid, ts, id) {
      var attrs, node;
      if (id == null) {
        id = null;
      }
      attrs = {
        to: jid,
        type: this.getJidType(jid),
        from: this.current_user_jid,
        id: Math.random() * 10000000 | 0
      };
      if (id !== null) {
        attrs['id'] = id;
      }
      node = $msg(attrs).c('body').t(text).up();
      node.c('replace', {
        xmlns: 'urn:xmpp:message-correct:0',
        id: original_mid,
        ts: ts
      }).up().c('x', {
        xmlns: 'http://hipchat.com'
      }).c('echo');
      return this.connection.send(node.tree());
    };

    HipChat.prototype.sendStateMessage = function(jid, type, state) {
      var attrs, node;
      attrs = {
        to: jid,
        type: type,
        id: Math.random() * 10000000 | 0
      };
      node = $msg(attrs).c(state, {
        xmlns: 'http://jabber.org/protocol/chatstates'
      }).up();
      return this.connection.send(node.tree());
    };

    HipChat.prototype.createRoom = function(name, topic, privacy, cb) {
      var pres, room_jid, to_slug;
      if (cb == null) {
        cb = this.noop;
      }
      to_slug = function(str) {
        var from, i, to, _i, _ref;
        str = str.replace(/^\s+|\s+$/g, "").toLowerCase();
        from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
        to = "aaaaeeeeiiiioooouuuunc------";
        for (i = _i = i, _ref = from.length; i <= _ref ? _i <= _ref : _i >= _ref; i = i <= _ref ? ++_i : --_i) {
          str = str.replace(new RegExp(from.charAt(i), "g"), to.charAt(i));
        }
        str = str.replace(/[^a-z0-9 -]/g, "").replace(/\s+/g, "-").replace(/-+/g, "-");
        return str;
      };
      room_jid = config.group_id + '_' + to_slug(name) + '@' + config.conference_server;
      pres = $pres({
        type: 'available',
        to: room_jid + '/' + config.user_name
      }).c('x', {
        xmlns: 'http://jabber.org/protocol/muc'
      }).c('history', {
        maxstanzas: 50
      }).up().up().c('x', {
        xmlns: 'http://jabber.org/protocol/muc#room'
      }).c('name').t(name).up().c('topic').t(topic).up().c('privacy').t(privacy);
      this.once('createRoom', cb);
      return this.connection.send(pres.tree());
    };

    HipChat.prototype.renameRoom = function(jid, new_name, cb) {
      var stanza;
      if (cb == null) {
        cb = this.noop;
      }
      Strophe.info('Renaming room');
      stanza = $iq({
        type: 'set',
        to: jid
      }).c('query', {
        xmlns: 'http://hipchat.com'
      }).c('rename').t(new_name);
      return this.sendIQ(stanza, cb);
    };

    HipChat.prototype.deleteRoom = function(jid, reason, cb) {
      var stanza;
      if (cb == null) {
        cb = this.noop;
      }
      Strophe.info('Deleting room');
      stanza = $iq({
        type: 'set',
        to: jid
      }).c('query', {
        xmlns: 'http://jabber.org/protocol/muc#owner'
      }).c('destroy').c('reason').t(reason);
      return this.sendIQ(stanza, cb);
    };

    HipChat.prototype.changeRoomPrivacy = function(jid, new_privacy, cb) {
      var stanza;
      if (cb == null) {
        cb = this.noop;
      }
      Strophe.info('Changing room privacy');
      stanza = $iq({
        type: 'set',
        to: jid
      }).c('query', {
        xmlns: 'http://hipchat.com'
      }).c('privacy').t(new_privacy);
      return this.sendIQ(stanza, cb);
    };

    HipChat.prototype.makeAPIRequest = function(func, opts, cb) {
      var payload;
      if (cb == null) {
        cb = this.noop;
      }
      payload = $.extend(opts, {
        user_id: config.user_id,
        group_id: config.group_id,
        token: config.token.token,
        format: 'json'
      });
      return $.ajax({
        type: 'POST',
        dataType: 'json',
        url: this.baseUrl + '/api/' + func,
        data: payload,
        async: true,
        success: cb
      });
    };

    HipChat.prototype.prefs_changed = false;

    HipChat.prototype.getBoolPref = function(name, default_val) {
      var ret, val;
      if (name in config.prefs) {
        val = config.prefs[name];
        ret = false;
        if (val === "true" || val === true) {
          ret = true;
        } else if (val === "false" || val === false) {
          ret = false;
        } else {
          ret = val;
        }
        return ret;
      }
      return default_val;
    };

    HipChat.prototype.getPref = function(name, default_val) {
      if (config.prefs[name]) {
        return config.prefs[name];
      }
      return default_val;
    };

    HipChat.prototype.savePreferences = function() {
      if (this.prefs_changed) {
        this.makeAPIRequest("save_preferences", {
          pref_data: config.prefs
        }, null);
        this.prefs_changed = false;
      }
    };

    HipChat.prototype.setPref = function(name, val, save_now) {
      config.prefs[name] = val;
      this.prefs_changed = true;
      if (save_now) {
        this.savePreferences();
      }
    };

    HipChat.prototype.sendUploadMessage = function(data) {
      var attrs, file_info, msg;
      if (!this.is_connected) {
        return false;
      }
      file_info = this.x2js.xml2json(data.file_info);
      attrs = {
        to: data.jid,
        type: data.type
      };
      msg = $msg(attrs).c('x', {
        xmlns: 'http://hipchat.com/protocol/muc#room'
      }).c('file', {
        'id': file_info.response.file_id
      }).up().up();
      if (data.type !== 'groupchat') {
        msg.c('x', {
          xmlns: 'http://hipchat.com'
        }).c('echo').up().up();
      }
      return this.connection.send(msg.tree());
    };

    HipChat.prototype.fetchFiles = function(jid, before, after, limit, cb) {
      var opts, stanza;
      if (before == null) {
        before = null;
      }
      if (after == null) {
        after = null;
      }
      if (limit == null) {
        limit = 50;
      }
      if (cb == null) {
        cb = this.noop;
      }
      Strophe.info('Requesting files');
      opts = {
        xmlns: 'http://hipchat.com/protocol/files',
        limit: limit
      };
      if (before != null) {
        opts.before = this.toXMPPDate(before);
      }
      stanza = $iq({
        type: 'get',
        to: jid
      }).c('query', opts);
      return this.sendIQ(stanza, cb);
    };

    HipChat.prototype.fetchLinks = function(jid, before, after, limit, cb) {
      var opts, stanza;
      if (before == null) {
        before = null;
      }
      if (after == null) {
        after = null;
      }
      if (limit == null) {
        limit = 50;
      }
      if (cb == null) {
        cb = this.noop;
      }
      Strophe.info('Requesting files');
      opts = {
        xmlns: 'http://hipchat.com/protocol/links',
        limit: limit
      };
      if (before != null) {
        opts.before = this.toXMPPDate(before);
      }
      stanza = $iq({
        type: 'get',
        to: jid
      }).c('query', opts);
      return this.sendIQ(stanza, cb);
    };

    HipChat.prototype.revokeOauth2Token = function() {
      var opts, stanza;
      opts = {
        xmlns: "http://hipchat.com/protocol/auth",
        action: "delete"
      };
      stanza = $iq({
        type: 'set'
      }).c('query', opts).c('type').t('oauth2');
      return this.sendIQ(stanza, this.noop);
    };

    HipChat.prototype.setTopic = function(jid, topic) {
      var attrs, msg;
      attrs = {
        to: jid,
        type: 'groupchat'
      };
      msg = $msg(attrs).c('subject').t(topic);
      return this.connection.send(msg.tree());
    };

    HipChat.prototype.setGuestAccess = function(jid, enabled) {
      var opts, stanza, val;
      val = enabled ? 1 : 0;
      opts = {
        xmlns: "http://hipchat.com"
      };
      stanza = $iq({
        to: jid,
        type: 'set'
      }).c('query', opts).c('guest_access').t(val);
      return this.sendIQ(stanza, this.noop);
    };

    HipChat.prototype.requestAddliveCredentials = function(jid, cb) {
      var done, fail, stanza;
      if (cb == null) {
        cb = this.noop;
      }
      stanza = $iq({
        to: jid,
        type: 'get'
      }).c('query', {
        xmlns: 'http://hipchat.com/protocol/addlive'
      });
      done = function(resp) {
        return cb(null, resp.query);
      };
      fail = function(resp) {
        return cb(resp.error);
      };
      return this.sendIQ(stanza, done, fail);
    };

    HipChat.prototype.sendVideoMessage = function(jid, type) {
      var attrs, node;
      if (type == null) {
        type = 'call';
      }
      if (!this.is_connected) {
        return false;
      }
      attrs = {
        to: jid,
        type: this.getJidType(jid),
        from: this.current_user_jid,
        id: Math.random() * 10000000 | 0
      };
      node = $msg(attrs).c('x', {
        xmlns: 'http://hipchat.com/protocol/addlive'
      }).c(type);
      return this.connection.send(node.tree());
    };

    return HipChat;

  })();

  if (typeof module !== 'undefined') {
    module.exports = HipChat;
  } else {
    window.HipChat = HipChat;
  }

}).call(this);

/**
 * @jsx React.DOM
 */
var RoomColumn, RoomColumns, RoomPanel;

RoomColumn = React.createClass({
  render: function(){
    return (
      <div className="room-col">
        <strong>{this.props.room.name}</strong>
        <ChatPanel jid={this.props.room.jid} />
      </div>
    );
  }
});

RoomColumns = React.createClass({
  render: function(){
    var roomNodes = this.props.rooms.map(function (room) {
      return <RoomColumn room={room} />;
    });
    return (
      <div className="room-cols">{roomNodes}</div>
    );
  }
});

RoomsPanel = React.createClass({
  getInitialState: function(){
    return { rooms: config.prefs.autoJoin };
  },
  render: function(){
    return (
      <div className="room-panel">
        <RoomColumns rooms={this.state.rooms} />
      </div>
    );
  }
});
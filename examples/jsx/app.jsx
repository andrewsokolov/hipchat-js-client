/**
 * @jsx React.DOM
 */
new HipChat(function(hc){
  window.hc = hc;
  hc.joinAutoJoinRooms();

  hc.on('message', function(msg){
    var jid = msg._from.split('/')[0];

    // We'll populate the chat panels through events
    $.event.trigger(jid+':message', msg);
  });

  hc.on('presence', function(presence){
    var jid = presence._from.split('/')[0];
    $.event.trigger(jid+':presence', presence);
    console.log(presence);
  });

  // React.renderComponent(
  //   <RoomsPanel />,
  //   document.getElementById('container')
  // );

  React.renderComponent(
    <RosterPanel />,
    document.getElementById('container')
  );
});

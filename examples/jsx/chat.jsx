/**
 * @jsx React.DOM
 */
var Message, MessageList, ChatPanel;

Message = React.createClass({
  render: function(){
    var from, stamp;
    if (!this.props.msg) { return; }
    from = this.props.msg._type == 'chat' ? this.props.msg._from : this.props.msg._from.split('/')[1];
    stamp = 'delay' in this.props.msg ? this.props.msg.delay._stamp : (new Date).toISOString();
    return (
      <div className="message">
        <div className="from">{from}</div>
        <div className="msg">{this.props.msg.body}</div>
        <div className="msg">{Date.create(stamp).relative()}</div>
      </div>
    );
  }
});

MessageList = React.createClass({
  render: function(){
    var messageNodes = this.props.messages.map(function (msg) {
      return <Message msg={msg} />;
    });
    return (
      <div>{messageNodes}</div>
    );
  }
});

ChatPanel = React.createClass({
  getInitialState: function(){
    return {messages: []}
  },
  componentWillMount: function(){
    // listen for new messages in this jid
    $(document).on(this.props.jid, this.handleNewMessage);
  },
  handleNewMessage: function(evt, msg){
    if (!('body' in msg)) { return; }
    this.state.messages.unshift(msg)
    this.setState({
      messages: this.state.messages
    });
  },
  render: function(){
    return (
      <div className="chat-panel">
        <textarea></textarea>
        <MessageList messages={this.state.messages} />
      </div>
    );
  }
});
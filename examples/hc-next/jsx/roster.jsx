/**
 * @jsx React.DOM
 */
var User, Users, UserList, RosterPanel;

CurrentUserAvatar  = React.createClass({
  getInitialState: function(){
    return {
      currentUser: {
        name: '',
        avatarUrl: ''
      }
    }
  },
  componentDidMount: function() {
    $(document).on('init-data', this.handleInitData);
    $(document).on(config.user_jid+':presence', this.handlePresenceChange);
  },
  handleInitData: function(evt, d){
    this.setState({
      currentUser: {
        name: d.name,
        avatarUrl: d.photo_small
      }
    });
  },
  handlePresenceChange: function(evt, presence){
    console.log($(this.getDOMNode()).parent().parent());
    $(this.getDOMNode()).parent().parent()
      .removeClass('hc-away')
      .removeClass('hc-dnd')
      .removeClass('hc-xa')
      .removeClass('hc-avail')
      .addClass('hc-'+presence.show);
  },
  render: function() {
    return (
      <div className="aui-avatar-inner user_photo">
        <img src={this.state.currentUser.avatarUrl} />
      </div>
    );
  }
});

User = React.createClass({
  getInitialState: function(){
    return {
      presence: {
        show: 'away',
        status: ''
      }
    };
  },
  componentDidMount: function() {
    // subscribe to presence here
    $(document).on(this.props.user._jid+':presence', this.handleNewPresence);
  },
  handleNewPresence: function(evt, presence){
    this.setState({
      presence: presence
    });
  },
  render: function(){
    return (
      <div className="user">
        <strong>{this.props.user._name}</strong>
        <span className={this.state.presence.show}>{this.state.presence.status}</span>
      </div>
    );
  }
});

UserList = React.createClass({
  render: function(){
    var userNodes = this.props.users.map(function(user) {
      return <User user={user} />;
    });
    return (
      <div className="user-list">{userNodes}</div>
    );
  }
});

RosterPanel = React.createClass({
  getInitialState: function(){
    return { users: [] };
  },
  componentWillMount: function() {
    var self = this;
    hc.fetchRoster(function(users){
      self.setState({
        users: users.query.item
      });
    });
  },
  render: function(){
    return (
      <div className="roster-panel">
        <UserList users={this.state.users} />
      </div>
    );
  }
});
/**
 * @jsx React.DOM
 */
new HipChat(function(hc){
  window.hc = hc;
  hc.joinAutoJoinRooms();

  React.renderComponent(
    <RoomsNav />,
    document.querySelector('.hc-rooms-nav')
  );

  React.renderComponent(
    <CurrentUserAvatar />,
    document.querySelector('.hc-current-user')
  );

  hc.fetchInitData(function(d){
    $.event.trigger('init-data', d.query);
  });

  hc.on('message', function(msg){
    var jid = msg._from.split('/')[0];
    $.event.trigger(jid+':message', msg);
  });

  hc.on('presence', function(presence){
    var jid = presence._from.split('/')[0];
    $.event.trigger(jid+':presence', presence);
  });
});

/**
 * @jsx React.DOM
 */
var Message, MessageList, ChatBox, ChatPanel;

Message = React.createClass({
  render: function(){
    var from, stamp;
    if (!this.props.msg) { return; }
    from = this.props.msg._type == 'chat' ? this.props.msg._from : this.props.msg._from.split('/')[1];
    stamp = 'delay' in this.props.msg ? this.props.msg.delay._stamp : (new Date).toISOString();
    return (
      <div className="hc-chat-row">
        <div className="hc-chat-from">{from}</div>
        <div className="hc-chat-msg">{this.props.msg.body}</div>
        <div className="hc-chat-time">{Date.create(stamp).relative()}</div>
      </div>
    );
  }
});

MessageList = React.createClass({
  componentDidMount: function() {
    $('#' + this.props.key + ' .hc-chat-history').height($('.aui-sidebar-body').height() - 150);
  },
  componentDidUpdate: function(prevProps, prevState) {
    this.scrollToNewMessage();
  },
  scrollToNewMessage: function(){
    var chatHeight = $('#' + this.props.key + ' .hc-chat-history').get(0).scrollHeight;
    // TODO don't scroll if not at the bottom
    $('#' + this.props.key + ' .hc-chat-history').scrollTop(chatHeight);
  },
  render: function(){
    var messageNodes = this.props.messages.map(function (msg) {
      return <Message msg={msg} />;
    });
    return (
      <div className="hc-chat-history">{messageNodes}</div>
    );
  }
});

ChatBox = React.createClass({
  handleReturnKey: function(evt){
    if (evt.keyCode === 13 && !evt.shiftKey) {
      this.sendMessage();
    }
  },
  sendMessage:function(){
    hc.sendMessage(this.props.jid, this.refs.message.getDOMNode().value.trim());
    this.refs.message.getDOMNode().value = '';
    return false;
  },
  render: function() {
    return (
      <form className="aui" onSubmit={this.sendMessage}>
        <span className="hc-text-input aui-buttons">
          <textarea className="textarea hc-textarea" ref="message" onKeyUp={this.handleReturnKey} />
          <button className="aui-button hc-emoticons"><span className="aui-icon aui-icon-small aui-iconfont-warning">Warning</span></button>
          <button className="aui-button hc-attach"><span className="aui-icon aui-icon-small aui-iconfont-attachment">Attachment</span></button>
        </span>
        <button className="aui-button aui-button-primary hc-send-message">Send</button>
      </form>
    );
  }
});

ChatPanel = React.createClass({
  getInitialState: function(){
    return {messages: []}
  },
  componentDidMount: function(){
    // listen for new messages in this jid
    $(document).on(this.props.jid+':message', this.handleNewMessage);
  },
  handleNewMessage: function(evt, msg){
    if (!('body' in msg)) { return; }
    this.state.messages.push(msg)
    this.setState({
      messages: this.state.messages
    });
  },
  render: function(){
    return (
      <div className="aui-page-panel chat-panel">
        <div className="aui-page-panel-inner">
          <section className="aui-page-panel-content">
            <MessageList messages={this.state.messages} key={this.props.key}/>
            <ChatBox jid={this.props.jid} />
          </section>
        </div>
      </div>
    );
  }
});
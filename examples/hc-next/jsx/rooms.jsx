/**
 * @jsx React.DOM
 */
var Room, Rooms, RoomHeader, RoomsNav, RoomsNavItem, RoomsNavItemRoom, RoomsNavItemPerson;

RoomHeader = React.createClass({
  render: function(){
    return (
      <header className="aui-page-header">
        <div className="aui-page-header-inner">
          <div className="aui-page-header-image">
            <div className="aui-avatar aui-avatar-xlarge aui-avatar-project">
              <div className="aui-avatar-inner">
                <img src="./img/team_blue.png"/>
              </div>
            </div>
          </div>
          <div className="aui-page-header-main chat-header">
            <h1>{this.props.room.name}</h1>
            <p>{this.props.details.query.x.topic}</p>
          </div>
          <div className="aui-page-header-actions">
            <div className="aui-buttons">
              <button className="aui-button"><span className="aui-icon aui-icon-small aui-iconfont-user">People </span> {this.props.details.query.x.num_participants}</button>
              <button className="aui-button"><span className="aui-icon aui-icon-small aui-iconfont-search">Search </span></button>
              <button className="aui-button"><span className="aui-icon aui-icon-small aui-iconfont-more">More </span></button>
            </div>
          </div>
        </div>
      </header>
    );
  }
});

Room = React.createClass({
  getInitialState: function(){
    return {
      details: {
        query: {
          x: {
            topic: '',
            num_participants: 1,
            privacy: 'public'
          }
        }
      }
    };
  },
  componentDidMount: function() {
    hc.fetchRoom(this.props.room.jid, this.handleRoomDetails);
  },
  handleRoomDetails: function(room){
    this.setState({
      details: room
    });
  },
  render: function(){
    var cx = React.addons.classSet;
    var hiddenRoomClass = cx({
      'hidden': config.prefs.chatToFocus !== this.props.room.jid
    });
    return (
      <div className={hiddenRoomClass + ' hc-room-panel'} id={this.props.key}>
        <RoomHeader room={this.props.room} details={this.state.details}/>
        <ChatPanel jid={this.props.room.jid} key={this.props.key} />
      </div>
    );
  }
});

Rooms = React.createClass({
  render: function(){
    var roomNodes = this.props.rooms.map(function (room) {
      return <Room room={room} key={btoa(room.jid).replace(/=/g,'')} />;
    });
    return (
      <div className="hc-room">{roomNodes}</div>
    );
  }
});

RoomsNavItemPerson = React.createClass({
  getInitialState: function(){
    return {
      user: {},
      presence: {
        show: 'unknown',
        status: ''
      }
    };
  },
  componentDidMount: function() {
    hc.fetchUserProfile(this.props.room.jid, this.handleUserProfileFetch);
    $(document).on(this.props.room.jid+':presence', this.handleNewPresence);
  },
  handleUserProfileFetch: function(user){
    this.setState({
      user: user.query
    })
  },
  handleNewPresence: function(evt, presence){
    presence.show = presence.show || 'available';
    this.setState({
      presence: presence
    });
  },
  render: function() {
    return (
      <a href="#" className="aui-nav-item" title={this.state.presence.status}>
        <span className="aui-icon hc-avatar"><img src={this.state.user.photo_small} /></span>
        <span className={"aui-icon aui-icon-small hc-status-icon hc-" + this.state.presence.show}></span>
        <span className="aui-nav-item-label">{this.props.room.name}</span>
      </a>
    );
  }
});

RoomsNavItemRoom = React.createClass({
  render: function() {
    return (
      <a href="#" className="aui-nav-item">
        <span className="aui-icon aui-icon-small aui-iconfont-comment"></span>
        <span className="aui-nav-item-label">{this.props.room.name}</span>
      </a>
    );
  }
});

RoomsNavItem  = React.createClass({
  switchRooms: function(evt){
    // TODO move these behaviors to their respective classes
    $('.hc-rooms-active li').removeClass('aui-nav-selected');
    $(this.getDOMNode()).addClass('aui-nav-selected');
    $('.hc-room-panel').addClass('hidden');
    $('#' + this.props.key).removeClass('hidden');
    return false;
  },
  render: function() {
    var cx = React.addons.classSet;
    var roomSelectedClass = cx({
      'aui-nav-selected': config.prefs.chatToFocus == this.props.room.jid
    });
    var roomLink = /@conf/.test(this.props.room.jid) ? <RoomsNavItemRoom room={this.props.room} /> : <RoomsNavItemPerson room={this.props.room} />;

    return (
      <li className={roomSelectedClass} onClick={this.switchRooms}>
        {roomLink}
      </li>
    );
  }
});

RoomsNav  = React.createClass({
  getInitialState: function(){
    return { rooms: _.uniq(config.prefs.autoJoin, function(a){ return a.jid }) };
  },
  render: function() {
    var roomNavNodes = this.state.rooms.map(function (room) {
      return <RoomsNavItem room={room} key={btoa(room.jid).replace(/=/g,'')}/>;
    });

    React.renderComponent(
      <Rooms rooms={this.state.rooms} />,
      document.querySelector('.hc-rooms-container')
    );

    return (
      <ul className="aui-nav hc-rooms-active">
        {roomNavNodes}
      </ul>
    );
  }
});
